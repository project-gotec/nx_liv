import Head from 'next/head';
import AuthServer from './api/auth';
import CidadesServer from './api/cidades';
import React, { useState, useContext, useEffect } from 'react';
import { Context } from "../context/AuthContext";
import { useRouter } from 'next/router';
import Link from 'next/link';
import ReactGA from 'react-ga';
import ReCAPTCHA from "react-google-recaptcha";
import MaskedInput from 'react-text-mask'
import {NotificationContainer, NotificationManager} from 'react-notifications';

export default function Register() {
    const router = useRouter();
    const {setUser, authenticated, setAuthenticated} = useContext(Context);
    const [cidades, setCidades] = useState([]);
    const [maskTel, setMaskTel] = useState(['(', /[1-9]/, /\d/, ')', ' ',/\d/,/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]);

    const [loading, setLoading] = useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    // const [telefone, setTelefone] = useState('');
    const [id_perfil, setPerfil] = useState('');
    const [instituicao, setInstituicao] = useState('');
    const [estado, setEstado] = useState('');
    // const [cidade, setCidade] = useState('');
    // const [qtde_alunos, setQuantidadeAlunos] = useState('');
    // const [id_tipo_instituicao, setTipoInstituicao] = useState('');
    // const [possui_liv, setPossuiLiv] = useState('');
    // const [como_foi_convidado, setComoFoiConvidado] = useState('');
    const [autorizacao_lgpd, setAutorizacaoLGPD] = useState(false);
    const [googleCaptcha, setGoogleCaptcha] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [errors, setErrors] = useState({});

    const getCidades = (uf) => {
        if(uf == ''){
            setCidades([]);
            return false;
        }

        if(uf != null){
            CidadesServer.list(uf).then(({data}) => setCidades(data));
        }
    }

    const changeEstado = (uf) => {
        setEstado(uf);
        getCidades(uf);
    }

    const handleRegister = (event) => {
        event.preventDefault();

        setLoading(true);

        AuthServer.register({
            id_perfil: id_perfil,
            id_tipo_instituicao: '',
            name: name,
            email: email,
            telefone: '',
            celular: '',
            instituicao: instituicao,
            estado: estado,
            cidade: '',
            qtde_alunos: '',
            possui_liv: '',
            como_foi_convidado: '',
            password: password,
            password_confirmation: passwordConfirm,
            autorizacao_lgpd: autorizacao_lgpd ? autorizacao_lgpd : null,
            google_recaptcha: googleCaptcha
        }).then(async ({data}) => {
            localStorage.setItem('user', JSON.stringify(data.user));

            await AuthServer.sendVerificationEmail(email);

            setUser(data.user);
            setAuthenticated(true);
            router.push('/');
        }).catch(({response}) => {
            if(response.data != undefined){
                const erros = Object.entries(response.data);
                
                erros.forEach(erro => {
                    NotificationManager.error(erro[1], 'Atenção');
                });
            }

            setErrors(response.data);
        }).finally(() => {
            setLoading(false);
        });
    }

    const validatorMessage = (input) => {
        let data = {
            invalid: false,
            message: '' 
        }

        if(errors.hasOwnProperty(input)){
            data.invalid = true;
            data.message = errors[input][0];
        }

        return data;
    }

    function checkMaskTel(value){
        setTimeout(() => {
            const isCel = (value.split('')[5] == '9') ? true : false;
            
            if(isCel) setMaskTel(['(', /[1-9]/, /\d/, ')', ' ',/\d/,/\d/,/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]);
            if(!isCel) setMaskTel(['(', /[1-9]/, /\d/, ')', ' ',/\d/,/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]);
        }, 400);
    }

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Registrar</title>
                <link rel="shortcut icon" href="/images/icons/favicon.png" />
            </Head>
            { 
                !authenticated && <div id="container-auth">
                    <div id="auth-form">
                        <form className="card form-congresso-liv" onSubmit={handleRegister}>
                            <input type="hidden" id="form-congresso-liv" value="" />

                            <div className="card-header text-center">
                                <div className="d-table d-md-none w-100 text-center">
                                    <h4>Cadastro na #SejaLIV</h4>
                                    <span>Que bom ter você por aqui!</span>
                                </div>
                                <div className="d-none d-md-table w-100 text-center">
                                    <h4>Cadastro na #SejaLIV</h4>
                                </div>
                            </div>
                            
                            <div className="card-body">
                                <div className="form-row">
                                    <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="nome">Nome completo</label>
                                            <input 
                                                id="nome" 
                                                type="text" 
                                                className={`form-control ${validatorMessage('name').invalid ? 'is-invalid' : ''}`}
                                                value={name}
                                                aria-describedby="nameFeedback"
                                                onChange={e => setName(e.target.value)} />
                                            {validatorMessage('name').invalid ? <div id="nameFeedback" className="invalid-feedback"> {validatorMessage('name').message} </div> : ''}
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="email">E-mail</label>
                                            <input 
                                                id="email" 
                                                type="email" 
                                                aria-describedby="emailFeedback"
                                                className={`form-control ${validatorMessage('email').invalid ? 'is-invalid' : ''}`}
                                                value={email}
                                                aria-describedby="emailFeedback"
                                                onChange={e => setEmail(e.target.value)} />
                                            {validatorMessage('email').invalid ? <div id="emailFeedback" className="invalid-feedback"> {validatorMessage('email').message} </div> : ''}
                                        </div>
                                    </div>
                                </div>
                                 <div className="form-row">
                                {/*    <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="telefone">Telefone</label>
                                            <MaskedInput 
                                                id="telefone"
                                                mask={maskTel} 
                                                guide={false}
                                                value={telefone}
                                                className={`form-control ${validatorMessage('telefone').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="telefoneFeedback"
                                                onChange={e => {
                                                    checkMaskTel(e.target.value);
                                                    setTelefone(e.target.value);
                                                }} />
                                            {validatorMessage('telefone').invalid ? <div id="telefoneFeedback" className="invalid-feedback"> {validatorMessage('telefone').message} </div> : ''}
                                        </div>
                                    </div> */}

                                    <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="id_perfil">Na comunidade escolar, eu sou...</label>
                                            <select 
                                                id="id_perfil" 
                                                className={`form-control ${validatorMessage('id_perfil').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="perfilFeedback"
                                                value={id_perfil} 
                                                onChange={e => setPerfil(e.target.value)}>
                                                <option value="">Selecione</option>
                                                <option value="1">Gestor(a) escolar / mantenedor(a)</option>
                                                <option value="2">Coordenador(a)</option>
                                                <option value="3">Professor(a)</option>
                                                <option value="4">Responsável de crianças e/ou adolescentes</option>
                                                <option value="5">Aluno(a)</option>
                                                <option value="7">Administrativo</option>
                                                <option value="8">Psicólogo(a) / Pedagogo(a)</option>
                                                <option value="6">Não estou relacionado(a) a uma escola</option>
                                            </select>
                                            {validatorMessage('id_perfil').invalid ? <div id="perfilFeedback" className="invalid-feedback"> {validatorMessage('id_perfil').message} </div> : ''}
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="instituicao">Nome da Escola</label>
                                            <input 
                                                id="instituicao" 
                                                type="text" 
                                                className={`form-control ${validatorMessage('instituicao').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="instituicaoFeedback"
                                                value={instituicao} 
                                                onChange={e => setInstituicao(e.target.value)}
                                                placeholder="Caso não pertença a nenhuma escola responda 'não tenho'" />
                                            {validatorMessage('instituicao').invalid ? <div id="instituicaoFeedback" className="invalid-feedback"> {validatorMessage('instituicao').message} </div> : ''}
                                        </div>
                                    </div>
                                </div> 

                                {/* <div className="form-row">
                                    <div className="col-12 col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="estado">Estado:</label>
                                            <select 
                                                id="estado"
                                                className={`form-control ${validatorMessage('estado').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="EstadoFeedback"
                                                value={estado} 
                                                onChange={e => changeEstado(e.target.value)}>
                                                <option value="">Selecione</option>
                                                <option value="AC">Acre</option>
                                                <option value="AL">Alagoas</option>
                                                <option value="AP">Amapá</option>
                                                <option value="AM">Amazonas</option>
                                                <option value="BA">Bahia</option>
                                                <option value="CE">Ceará</option>
                                                <option value="DF">Distrito Federal</option>
                                                <option value="ES">Espírito Santo</option>
                                                <option value="GO">Goiás</option>
                                                <option value="MA">Maranhão</option>
                                                <option value="MT">Mato Grosso</option>
                                                <option value="MS">Mato Grosso do Sul</option>
                                                <option value="MG">Minas Gerais</option>
                                                <option value="PA">Pará</option>
                                                <option value="PB">Paraíba</option>
                                                <option value="PR">Paraná</option>
                                                <option value="PE">Pernambuco</option>
                                                <option value="PI">Piauí</option>
                                                <option value="RJ">Rio de Janeiro</option>
                                                <option value="RN">Rio Grande do Norte</option>
                                                <option value="RS">Rio Grande do Sul</option>
                                                <option value="RO">Rondônia</option>
                                                <option value="RR">Roraima</option>
                                                <option value="SC">Santa Catarina</option>
                                                <option value="SP">São Paulo</option>
                                                <option value="SE">Sergipe</option>
                                                <option value="TO">Tocantins</option>
                                            </select>
                                            {validatorMessage('estado').invalid ? <div id="EstadoFeedback" className="invalid-feedback"> {validatorMessage('estado').message} </div> : ''}
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="cidade">Cidade:</label>
                                            <select 
                                                id="cidade"
                                                className={`form-control ${validatorMessage('cidade').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="CidadeFeedback"
                                                value={cidade} 
                                                onChange={e => setCidade(e.target.value)}>
                                                <option value="">Selecione</option>
                                                {
                                                    cidades.map((item, index) => <option value={item.name} key={index}>{item.name}</option>)
                                                }
                                            </select>
                                            {validatorMessage('cidade').invalid ? <div id="CidadeFeedback" className="invalid-feedback"> {validatorMessage('cidade').message} </div> : ''}
                                        </div>
                                    </div>
                                </div> */}

                                <div className="form-row">
                                    {/* <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="qtde_alunos">Quantidade de alunos:</label>
                                            <select 
                                                id="qtde_alunos"
                                                className={`form-control ${validatorMessage('qtde_alunos').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="AlunosFeedback"
                                                value={qtde_alunos} 
                                                onChange={e => setQuantidadeAlunos(e.target.value)}>
                                                <option value="">Selecione</option>
                                                <option value="Até 300">Até 300</option>
                                                <option value="301 - 600">301 - 600</option>
                                                <option value="601 - 1000">601 - 1000</option>
                                                <option value="Acima de 1000">Acima de 1000</option>
                                                <option value="Não sei informar">Não sei informar</option>
                                            </select>
                                            {validatorMessage('qtde_alunos').invalid ? <div id="AlunosFeedback" className="invalid-feedback"> {validatorMessage('qtde_alunos').message} </div> : ''}
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="id_tipo_instituicao">Escola pública ou privada?</label>
                                            <select 
                                                id="id_tipo_instituicao"
                                                className={`form-control ${validatorMessage('id_tipo_instituicao').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="TipoInstituicaoFeedback"
                                                value={id_tipo_instituicao} 
                                                onChange={e => setTipoInstituicao(e.target.value)}>
                                                <option value="">Selecione</option>
                                                <option value="1">Pública</option>
                                                <option value="2">Privada</option>
                                                <option value="3">ONG</option>
                                                <option value="4">Não tenho relação com uma escola</option>
                                            </select>
                                            {validatorMessage('id_tipo_instituicao').invalid ? <div id="TipoInstituicaoFeedback" className="invalid-feedback"> {validatorMessage('id_tipo_instituicao').message} </div> : ''}
                                        </div>
                                    </div> */}
                                    {/* <div className="col-12 col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="possui_liv">Tem LIV na sua escola?</label>
                                            <select 
                                                id="possui_liv"
                                                className={`form-control ${validatorMessage('possui_liv').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="PossuiLivFeedback"
                                                value={possui_liv} 
                                                onChange={e => setPossuiLiv(e.target.value)}>
                                                <option value="">Selecione</option>
                                                <option value="Sim">Sim</option>
                                                <option value="Não">Não</option>
                                                <option value="Não, mas tenho interesse em ter o LIV!">Não, mas tenho interesse em ter o LIV!</option>
                                            </select>
                                            {validatorMessage('possui_liv').invalid ? <div id="PossuiLivFeedback" className="invalid-feedback"> {validatorMessage('possui_liv').message} </div> : ''}
                                        </div>
                                    </div> */}
                                    {/* <div className="col-12">
                                        <div className="form-group">
                                            <label htmlFor="como_foi_convidado">Como foi convidado para o Seja LIV?</label>
                                            <select 
                                                id="como_foi_convidado"
                                                className={`form-control ${validatorMessage('como_foi_convidado').invalid ? 'is-invalid' : ''}`} 
                                                aria-describedby="ComoFoiConvidadoFeedback"
                                                value={como_foi_convidado} 
                                                onChange={e => setComoFoiConvidado(e.target.value)}>
                                                <option value="">Selecione</option>
                                                <option value="Pela minha escola">Pela minha escola</option>
                                                <option value="Pelas redes sociais/ Google">Pelas redes sociais/ Google</option>
                                                <option value="Pelo consultor LIV">Pelo consultor LIV</option>
                                                <option value="Plataforma Eleva">Plataforma Eleva</option>
                                                <option value="Pela Agenda Edu">Pela Agenda Edu</option>
                                                <option value="Outros">Outros</option>
                                            </select>
                                            {validatorMessage('como_foi_convidado').invalid ? <div id="ComoFoiConvidadoFeedback" className="invalid-feedback"> {validatorMessage('como_foi_convidado').message} </div> : ''}
                                        </div>
                                    </div> */}
                                </div>

                                <div className="form-group">
                                    <label htmlFor="password">Crie sua senha</label>
                                    <input 
                                        id="password" 
                                        type="password" 
                                        placeholder="Digite uma senha" 
                                        className={`form-control ${validatorMessage('password').invalid ? 'is-invalid' : ''}`} 
                                        aria-describedby="PasswordFeedback"
                                        value={password} 
                                        onChange={e => setPassword(e.target.value)} />
                                    {validatorMessage('password').invalid ? <div id="PasswordFeedback" className="invalid-feedback"> {validatorMessage('password').message} </div> : ''}
                                </div>

                                <div className="form-group">
                                    <label htmlFor="password-confirm">Confirme sua senha</label>
                                    <input 
                                        id="password-confirm" 
                                        type="password" 
                                        placeholder="Digite novamente a senha" 
                                        className={`form-control ${validatorMessage('password_confirmation').invalid ? 'is-invalid' : ''}`} 
                                        aria-describedby="PasswordConfirmationFeedback"
                                        value={passwordConfirm} 
                                        onChange={e => setPasswordConfirm(e.target.value)} />
                                    {validatorMessage('password_confirmation').invalid ? <div id="PasswordConfirmationFeedback" className="invalid-feedback"> {validatorMessage('password_confirmation').message} </div> : ''}
                                </div>
                            </div>
                            <div className="card-footer  text-center bg-transparent">
                                <div className="form-row">
                                    <div className="col-12">
                                        <div className="form-group d-flex flex-column justify-content-center align-items-center">
                                            <div className="custom-control custom-checkbox">
                                                <input 
                                                    type="checkbox" 
                                                    className={`custom-control-input ${validatorMessage('autorizacao_lgpd').invalid ? 'is-invalid' : ''}`} 
                                                    id="communications" 
                                                    aria-describedby="LGPDFeedback"
                                                    value={autorizacao_lgpd}
                                                    onChange={e => setAutorizacaoLGPD(!autorizacao_lgpd)} />
                                                <label className="custom-control-label" style={{fontSize: '0.8rem'}} htmlFor="communications">
                                                Autorizo o recebimento de conteúdos sobre educação socioemocional por e-mail e telefone e o tratamento dos meus dados pessoais pelo LIV, que observará o regime legal aplicável à proteção de dados pessoais no Brasil, notadamente a Lei 13.709/2018 – “LGPD”.
                                                <br/>
                                                Ao informar meus dados eu concordo com a <a href="https://elevaeducacao.com.br/wp-content/uploads/2021/03/Politica_de_Privacidade_de_Dados.pdf" target="_blank">Política de Privacidade de Dados</a> do Grupo Eleva.
                                                </label>
                                                {validatorMessage('autorizacao_lgpd').invalid ? <div id="LGPDFeedback" className="invalid-feedback"> {validatorMessage('autorizacao_lgpd').message} </div> : ''}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex flex-column align-items-center justify-content-center mb-2">
                                    <ReCAPTCHA sitekey={process.env.APP_RECAPTCHA_GOOGLE} onChange={value => setGoogleCaptcha(value)} onExpired={() => setGoogleCaptcha('')} />
                                    {validatorMessage('google_recaptcha').invalid ? <div className="alert alert-danger mt-2" role="alert">{validatorMessage('google_recaptcha').message}</div> : ''}
                                </div>
                                <button type="submit" className="btn btn-login btn-block mb-2" id="btn-login" disabled={loading}>
                                    {loading
                                        ? <div className="spinner-border spinner-border-sm mr-2" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                        : ''}
                                    {loading 
                                    ? 'Enviando' 
                                    : 'Enviar'}
                                </button>
                                <strong className="d-flex align-items-center justify-content-center">Já está cadastrado?<Link href="/login"><a className="btn btn-link p-0 ml-1">Fazer login</a></Link></strong>
                            </div>
                        </form>
                    </div>
                    <div id="auth-illustration" style={{ backgroundImage: 'url(https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/banner_login.jpg)' }}></div>
                </div>
            }
            <NotificationContainer/>
        </>
    )
}