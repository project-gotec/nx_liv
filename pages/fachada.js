import React, { useContext, useState, useEffect } from 'react';
// import { Modal, Button } from 'react-bootstrap';
import Head from 'next/head';
// import { Context } from '../context/AuthContext';
import Link from 'next/link';
import ReactGA from 'react-ga';

export default function Fachada() {
    // const {authenticated, checkAuthenticated} = useContext(Context);

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME }</title>
            </Head>
            <div className="pages">
                <Link href="/lobby">
                    <a style={ { position: 'relative', width: '100%' } }>
                        <img src="/images/3d/fachada.jpg" className="img-background" height="1062" width="2000" />
                        <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                            <polygon className="polygon" points="798.98,303.43 1081.36,230.4 1032.34,713.72 1015.74,709.64 988.17,703.26 959.32,697.38 
                                    929.19,693.04 900.34,688.45 871.32,685.98 871.15,679.09 872.94,677.47 874.21,671.51 872.26,668.02 868.43,665.38 867.91,660.53 
                                    864.43,658.23 860.94,659.77 859.74,663.09 860.6,666.49 860.77,668.7 858.64,672.45 858.38,681.13 858.47,684.87 848,682.74 
                                    848.17,679.34 847.66,676.45 845.28,673.13 846.21,669.3 842.81,666.49 839.06,669.89 839.4,674.83 837.45,678.23 836.6,681.04 
                                    835.57,678.91 835.06,671.77 835.66,661.04 834.47,654.32 835.66,651.94 833.19,649.3 829.36,650.91 829.53,663.51 828.94,679.6 
                                    828.26,680.62 803.83,678.66 772.68,676.7 743.83,675.68 737.7,675.17 "/>
                        </svg>
                    </a>
                </Link>
            </div>
        </>
    )
}