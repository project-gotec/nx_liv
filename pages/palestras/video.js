import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import { useRouter } from 'next/router';

import EventoServer from '../api/evento';

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function TenhaLivVideo() {
    const router = useRouter();
    const [ evento, setEvento ] = useState( null );

    const getEvento = async () => {
        const { data } = await EventoServer.show();

        if ( data != undefined ) {
            if ( data.img_plenaria != 0 ) {
                router.push( '/palestras' );
                return false;
            }
        }

        setEvento( data );
    }

    useEffect( () => {
        // getEvento();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Tenha liv - Video</title>
            </Head>
            {
                evento && <div id="page-video">
                    <div className="video shadow-sm">
                        <iframe src={ evento.lk_video } frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    </div>
                    <div className="chat">
                        <iframe src={ evento.lk_chat } height="100%" width="100%" frameBorder="0"></iframe>
                    </div>
                    <div className="navgation" style={ { top: 100 } }>
                        <Link href="/palestras">
                            <a className="btn btn-light shadow-sm"><FaArrowLeft size={ 20 } /></a>
                        </Link>
                    </div>
                </div>
            }
        </>
    )
}