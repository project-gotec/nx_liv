import React, { useContext, useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function Palestras() {
    const [ modal, setModal ] = useState( false );

    useEffect( () => {
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Palestras</title>
            </Head>
            <div className="pages">
                    <div className="navgation">
                        <Link href="/lobby">
                            <a className="btn btn-back"><FaArrowLeft size={ 22 } /></a>
                        </Link>
                    </div>
                    <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                    <a href="https://zoom.us/j/91746009066">
                            <polygon className="polygon" points="684.5,686.5 618.5,686.5 618.5,282.5 737.5,282.5 822.5,282.5 898.5,282.5 998.5,282.5 1383.5,282.5 1385.5,282.5 1385.5,686.5 " />
                        </a>
                    </svg>
                    
                    <img src="../images/3d/bg-auditorio.jpg" className="img-background" height="1062" width="2000" />
                    { modal && <img src="../images/3d/Popup-com-fundo.png" className="img-background background-popup" height="1062" width="2000" /> }
                </div>
        </>
    )
}