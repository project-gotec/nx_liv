import React, {useContext, useEffect} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import ReactGA from 'react-ga';
import { FaArrowLeft } from "react-icons/fa";

export default function VideoLiv() {
    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | escola que sente</title>
            </Head>
            <div className="pages">
                    <div className="video">
                        <iframe src="https://www.youtube.com/embed/6PMjT0_Rj2Q" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen></iframe>
                    </div>
                    <div className="navgation">
                        <Link href="/lobby">
                            <a className="btn btn-back mt-5"><FaArrowLeft size={22} /></a>
                        </Link>
                    </div>
                </div>
        </>
    )
}