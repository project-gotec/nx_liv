import Client from './client';

const Cidades = {
    list: (uf) => Client.get(`/cidades/${uf}`),
};

export default Cidades;