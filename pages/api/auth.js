import Client from './client';

const Auth = {
    login: (params) => Client.post('/login', params),
    register: (params) => Client.post('/register', params),
    reset: (key, params) => Client.put(`/emails/reset-password/${key}`, params),
    sendResetEmail: (email) => Client.post(`/emails/reset-password/${email}`),
    sendVerificationEmail: (email) => Client.post(`/emails/send-verification-email/${email}`),
    setVerificationEmail: (key) => Client.put(`/emails/set-verification-email/${key}`),
};
export default Auth;