import axios from 'axios';

const Client = axios.create({
    baseURL: process.env.APP_API
});

export default Client;