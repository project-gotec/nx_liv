import Head from 'next/head';
import AuthServer from './api/auth';
import React, { useState, useContext, useEffect } from 'react';
import { Context } from "../context/AuthContext";
import { useRouter } from 'next/router';
import ReactGA from 'react-ga';

export default function ResetarSenha() {
    const router = useRouter();
    const {setUser, authenticated, setAuthenticated, checkAuthenticated} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [errors, setErrors] = useState({});
    const [successMessage, setSuccessMessage] = useState('');

    const handleReset = (event) => {
        event.preventDefault();

        setLoading(true);
        
        if(router.query.key != undefined){
            const key = router.query.key.replace(/\//g, "");

            if(password != passwordConfirm){
                setErrors({
                    password_confirmation: ['As senhas precisam ser iguais.']
                });
                setLoading(false);
                return false;
            }
    
            AuthServer.reset(key, {
                password: password
            }).then(({data}) => {
                localStorage.setItem('user', JSON.stringify(data.user));
    
                setUser(data.user);
                setAuthenticated(true);
                window.location = '/';
            }).catch(({response}) => {
                setErrors(response.data);
            }).finally(() => {
                setLoading(false);
            });
        }
    }

    const validatorMessage = (input) => {
        let data = {
            invalid: false,
            message: '' 
        }

        if(errors.hasOwnProperty(input)){
            data.invalid = true;
            data.message = errors[input][0];
        }

        return data;
    }

    useEffect(() => {
        checkAuthenticated();
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, [router.query.key]);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Resetar minha senha</title>
            </Head>
            <div id="container-auth">
                <div id="auth-form" className="d-flex flex-column justify-content-center">
                    <div className="card" id="card-auth">
                        <div className="card-header text-center">
                            <img src="/images/logo.png" className="img-fluid mb-3" />
                        </div>
                        <div className="card-body">
                            { successMessage != '' ? <div className="alert alert-success" role="alert">{successMessage}</div> : ''}
                            <form onSubmit={ handleReset }>
                                <div className="form-group">
                                    <label htmlFor="password">Nova senha</label>
                                    <input 
                                        id="password" 
                                        type="password" 
                                        placeholder="Digite uma senha" 
                                        className={`form-control ${validatorMessage('password').invalid ? 'is-invalid' : ''}`} 
                                        aria-describedby="PasswordFeedback"
                                        value={password} 
                                        onChange={e => setPassword(e.target.value)} />
                                    {validatorMessage('password').invalid ? <div id="PasswordFeedback" className="invalid-feedback"> {validatorMessage('password').message} </div> : ''}
                                </div>

                                <div className="form-group">
                                    <label htmlFor="password-confirm">Confirme sua senha</label>
                                    <input 
                                        id="password-confirm" 
                                        type="password" 
                                        placeholder="Digite novamente a senha" 
                                        className={`form-control ${validatorMessage('password_confirmation').invalid ? 'is-invalid' : ''}`} 
                                        aria-describedby="PasswordConfirmationFeedback"
                                        value={passwordConfirm} 
                                        onChange={e => setPasswordConfirm(e.target.value)} />
                                    {validatorMessage('password_confirmation').invalid ? <div id="PasswordConfirmationFeedback" className="invalid-feedback"> {validatorMessage('password_confirmation').message} </div> : ''}
                                </div>
                                <button type="submit" className="btn btn-success btn-block d-flex align-items-center justify-content-center" disabled={loading}>
                                    {loading ? <div className="spinner-border spinner-border-sm mr-2" role="status"><span className="sr-only">Loading...</span></div> : ''}
                                    {loading ? <span>Salvando</span> : <span>Salvar</span>}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="auth-illustration" style={{ backgroundImage: 'url(https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/banner_login.jpg)' }}></div>
            </div>
        </>
    )
}