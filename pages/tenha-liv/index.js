import React, {useContext, useState, useEffect} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import ReactGA from 'react-ga';
import { FaArrowLeft } from "react-icons/fa";

export default function TenhaLiv() {
    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Tenha liv</title>
            </Head>
            <div className="pages">
                    <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                        <Link href="/tenha-liv/video">
                            <a>
                                <polygon id="tela" className="polygon" points="614.64,141.89 616.34,668.19 1556.26,669.21 1559.66,141.89 "/>
                            </a>
                        </Link>
                        <a href="https://www.inteligenciadevida.com.br/pt/queremosliv/seja-uma-escola-parceira/#formulario">
                            <polygon id="quero-ser-parceiro" className="polygon" points="1591.23,301.38 1591.23,212.79 1595.83,202.32 1602.98,194.4 1614.98,188.28 1933.62,170.4 
                            1932.6,289.13 " />
                        </a>
                    </svg>
                    <div className="navgation">
                        <Link href="/lobby">
                            <a className="btn btn-back"><FaArrowLeft size={22} /></a>
                        </Link>
                    </div>
                    <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/3d/tenha-liv.jpg" className="img-background" height="1062" width="2000" />
                </div>
        </>
    )
}