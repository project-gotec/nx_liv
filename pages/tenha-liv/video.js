import React, {useContext, useState, useEffect} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import ReactGA from 'react-ga';
import { FaArrowLeft } from "react-icons/fa";

export default function TenhaLivVideo() {
    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Tenha liv - Video</title>
            </Head>
             <div className="pages">
                    <div className="video">
                        <iframe src="https://www.youtube.com/embed/2ZS-1WAww-0" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen></iframe>
                    </div>
                    <div className="navgation">
                        <Link href="/tenha-liv">
                            <a className="btn btn-back mt-5"><FaArrowLeft size={22} /></a>
                        </Link>
                    </div>
                </div>
        </>
    )
}