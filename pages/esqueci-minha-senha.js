import Head from 'next/head';
import AuthServer from './api/auth';
import React, { useState, useContext, useEffect } from 'react';
import { Context } from "../context/AuthContext";
import { useRouter } from 'next/router';
import ReactGA from 'react-ga';

export default function EsqueciMinhaSenha() {
    const router = useRouter();
    const {authenticated} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [errors, setErrors] = useState({});
    const [successMessage, setSuccessMessage] = useState('');

    const handleSend = (event) => {
        event.preventDefault();

        setLoading(true);

        if(email == ''){
            setErrors({
                email:  ['Informe seu e-mail.']
            });

            setLoading(false);
            
            return false;
        }

        AuthServer.sendResetEmail(email).then(({data}) => {
            setSuccessMessage(data);
        }).catch(({response}) => {
            setErrors(response.data);
        }).finally(() => {
            setLoading(false);
        });
    }

    const validatorMessage = (input) => {
        let data = {
            invalid: false,
            message: '' 
        }

        if(errors.hasOwnProperty(input)){
            data.invalid = true;
            data.message = errors[input][0];
        }

        return data;
    }

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Esqueci minha senha</title>
            </Head>
            <div id="container-auth">
                <div id="auth-form" className="d-flex flex-column justify-content-center">
                    <div className="card" id="card-auth">
                        <div className="card-header text-center">
                            <img src="/images/logo.png" className="img-fluid mb-3" />
                        </div>
                        <div className="card-body">
                            { successMessage != '' ? <div className="alert alert-success" role="alert">{successMessage}</div> : ''}
                            <form onSubmit={ handleSend } >
                                <div className="form-group">
                                    <label htmlFor="email">Informe seu e-mail:</label>
                                    <input type="email" className={`form-control ${validatorMessage('email').invalid ? 'is-invalid' : ''}`} id="email" aria-describedby="emailFeedback" value={email} onChange={e => setEmail(e.target.value)} />
                                    {validatorMessage('email').invalid ? <div id="emailFeedback" className="invalid-feedback">{validatorMessage('email').message}</div> : ''}
                                </div>
                                <button type="submit" className="btn btn-success btn-block d-flex align-items-center justify-content-center" disabled={loading}>
                                    {loading ? <div className="spinner-border spinner-border-sm mr-2" role="status"><span className="sr-only">Loading...</span></div> : ''}
                                    {loading ? <span>Solicitando</span> : <span>Solicitar link para redefinir senha</span>}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="auth-illustration" style={{ backgroundImage: 'url(https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/banner_login.jpg)' }}></div>
            </div>
        </>
    )
}