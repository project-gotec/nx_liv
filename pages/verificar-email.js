import Head from 'next/head';
import AuthServer from './api/auth';
import React, { useState, useEffect, useContext } from 'react';
import { Context } from "../context/AuthContext";
import { useRouter } from 'next/router';
import ReactGA from 'react-ga';

export default function VerificarEmail() {
    const router = useRouter();
    const {user, authenticated, checkAuthenticated} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [errors, setErrors] = useState({});
    const [successMessage, setSuccessMessage] = useState('');

    const validatorMessage = (input) => {
        let data = {
            invalid: false,
            message: ''
        }

        if (errors.hasOwnProperty(input)) {
            data.invalid = true;
            data.message = errors[input][0];
        }

        return data;
    }

    const handleSendEmailVerification = (event) => {
        event.preventDefault();

        setLoading(true);

        AuthServer.sendVerificationEmail(user.email).then(response => {
            setSuccessMessage('Link de verificação enviado.');
            setErrors({});
        }).catch(error => {
            setSuccessMessage('');
            setErrors({
                email: ['Houve um problema ao enviar o link, tente novamente em alguns segundos.']
            });
        }).finally(() => {
            setLoading(false);
        });
    }

    useEffect(() => {
        checkAuthenticated();
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Verificação</title>
            </Head>
            <div id="container-auth">
                <div id="auth-form" className="d-flex flex-column justify-content-center">
                    <div className="card" id="card-auth">
                        <div className="card-header text-center">
                            <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/logo.png" className="img-fluid d-none d-md-block mb-3" />
                            <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/logo-amarelo.png" className="img-fluid d-block d-md-none mb-3" />
                        </div>
                        <div className="card-body">
                            { validatorMessage('email').invalid ? <div className="alert alert-danger text-center" role="alert">{validatorMessage('email').message}</div> : '' }
                            { successMessage != '' ? <div className="alert alert-success text-center" role="alert">{successMessage}</div> : ''}

                            <form onSubmit={ handleSendEmailVerification } >
                                <p className="text-center m-0">Um novo link de verificação foi enviado para seu e-mail.</p>
                                <p className="text-center m-0">Apenas após esse passo, sua inscrição estará completa. Antes de prosseguir, por favor, veja se recebeu o link de verificação em seu e-mail.</p>
                                <p className="text-center">Se você não recebeu o e-mail clique no botão <strong>"Solicitar outro"</strong></p>
                                <button type="submit" className="btn btn-success btn-block d-flex align-items-center justify-content-center" disabled={loading}>
                                    {loading ? <div className="spinner-border spinner-border-sm mr-2" role="status"><span className="sr-only">Loading...</span></div> : ''}
                                    {loading ? <span>Solicitando</span> : <span>Solicitar outro</span>}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="auth-illustration" style={{ backgroundImage: 'url(/images/banner_login.jpg)' }}></div>
            </div>
        </>
    )
}