import Head from 'next/head';
import AuthServer from './api/auth';
import React, { useState, useEffect, useContext } from 'react';
import { Context } from "../context/AuthContext";
import { useRouter } from 'next/router';
import Link from 'next/link';
import ReactGA from 'react-ga';

export default function ValidarEmail() {
    const router = useRouter();
    const {user, authenticated, checkAuthenticated, setUser} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [success, setSuccess] = useState(false);

    const handleValidate = () => {
        setLoading(true);

        
        if(router.query.key != undefined){
            const key = router.query.key.replace(/\//g, "");

            AuthServer.setVerificationEmail(key).then(response => {
                localStorage.setItem('user', JSON.stringify(response.data.data));
                setUser(response.data.data);
                setSuccess(true);
            }).catch(error => {
                setSuccess(false);
                console.log(error);
            }).finally(() => {
                setLoading(false);
            });
        }
    }

    const checkSuccessMessage = () => (
        success ? <div className="alert alert-success text-center" role="alert">Seu e-mail foi confirmado com sucesso. <Link href="/">Clique aqui para voltar ao site.</Link></div> : <div className="alert alert-warning text-center" role="alert">Houve um problema ao confirmar seu email. <Link href="/verificar-email">Clique aqui para solicitar outro email.</Link></div>
    )

    useEffect(() => {
        checkAuthenticated();
        handleValidate();
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, [router.query.key]);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Validação</title>
            </Head>
            <div id="container-auth">
                <div id="auth-form" className="d-flex flex-column justify-content-center">
                    <div className="card" id="card-auth">
                        <div className="card-header text-center">
                            <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/logo.png" className="img-fluid d-none d-md-block mb-3" />
                            <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/logo-amarelo.png" className="img-fluid d-block d-md-none mb-3" />
                        </div>
                        <div className="card-body">
                            {
                                loading ? <div className="d-flex justify-content-center align-items-center">
                                    <div className="spinner-border" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                    <span className="ml-2">Validando...</span>
                                </div>
                                :
                                checkSuccessMessage()
                            }
                        </div>
                    </div>
                </div>
                <div id="auth-illustration" style={{ backgroundImage: 'url(/images/banner_login.jpg)' }}></div>
            </div>
        </>
    )
}