import React, { useContext, useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';

// import { Context } from '../context/AuthContext';

import ReactGA from 'react-ga';

export default function Lobby() {
    // const { authenticated, checkAuthenticated } = useContext( Context );

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Lobby</title>
            </Head>
            <div className="pages">
                <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                    <Link href="/tenha-liv">
                        <a><polygon id="tenha-liv" className="polygon" points="615.5,388.5 607.5,248.5 609.5,234.5 618.5,217.5 630.5,203.5 642,195 653,190 
                            664,188 677,187 690,188 700,190 714,196 727,205 738,215 746,226 753,239 757,253 759,258 763,389 "/></a>
                    </Link>

                    <Link href="/video-liv">
                        <a><polygon id="video" className="polygon" points="869,198 872,345 1115,344 1116,342 1115,340 1115,338 1117,333 1120,333 1123,199 " /></a>
                    </Link>

                    <Link href="/guia-acolhimento">
                        <a><polygon id="guia-acolhimento" className="polygon" points="1229,389 1233,261 1236,248 1241,236 1247,225 1255,216 1262,209 1271,202 
                            1282,196 1295,192 1303,190 1316,189 1328,190 1337,193 1347,197 1356,203 1364,210 1370,217 1373,222 1377,230 1380,238 1382,246 
                            1375,389 "/></a>
                    </Link>

                    <Link href="/quem-esta-com-o-liv">
                        <a><polygon id="agenda" className="polygon" points="629,618 623,515 623,483 627,468 634,455 644,444 653,436 663,431 675,427 686,424 
                            703,424 719,428 731,433 741,440 748,446 755,454 760,462 764,472 767,483 772,606 "/></a>
                    </Link>

                    <Link href="/palestras">
                        <a><polygon id="palestras" className="polygon" points="939,596 939,480 941,468 945,459 950,450 956,443 962,437 969,432 976,428 983,425 
                            991,423 998,422 1011,422 1022,424 1035,429 1043,434 1051,441 1056,446 1061,453 1064,459 1067,466 1069,474 1070,480 1069,598 
                            975,599 "/></a>
                    </Link>

                    <Link href="/exposicao">
                        <a><polygon id="exposicao" className="polygon" points="1221,604 1226,485 1229,470 1234,460 1241,451 1248,443 1256,437 1264,432 1272,428 
                            1282,425 1290,424 1309,424 1321,427 1335,433 1345,441 1353,448 1360,459 1364,466 1367,476 1368,487 1362,615 1241,606 1240,602 
                            1240,600 1242,598 1243,595 1240,591 1235,593 1234,597 1236,599 1235,604 1234,605 "/></a>
                    </Link>
                </svg>
                <img src="../images/3d/lobby.jpg" className="img-background" height="1062" width="2000" />
            </div>

        </>
    )
}