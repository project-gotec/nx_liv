import React, { useContext, useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';

import { Context } from '../context/AuthContext';

import ReactGA from 'react-ga';
import { FaFacebookF, FaLinkedinIn, FaInstagram, FaYoutube, FaMousePointer } from "react-icons/fa";

export default function Home() {
    const { authenticated } = useContext( Context );

    useEffect( () => {
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME }</title>
            </Head>

            <section id="banner">
                { authenticated && <img src="./images/banner-logado.jpg" className="img-fluid" alt="Banner logado" /> }
                { !authenticated && <img src="./images/banner.jpg" className="img-fluid" alt="Banner" /> }
            </section>

            {!authenticated && <section id="description" className="shadow-sm">
                <div className="container">
                    <div className="form-row">
                        <div className="col-12 col-md-6">
                            <div id="text">
                                <h2 className="text-uppercase mb-3">BEM-VINDO(A)!</h2>
                                <p className="mb-0">Somos o Laboratório Inteligência de Vida, programa que desenvolve o pilar socioemocional nas escolas de todo o Brasil. Criamos espaços de fala e de escuta para ampliar a compreensão que os alunos têm de si, do outro e do mundo.</p>

                                <p>Essa é a plataforma 3D "Seja LIV", nossa experiência inovadora e exclusiva. Aqui, a comunidade escolar se reúne e vivencia os currículos do nosso programa socioemocional. Você
                                pode navegar junto com os personagens, visitar a casa amarela, ler as histórias do Tomás, conhecer nossos jogos e seriados e muito mais!
                                </p>
                                <p>Acesse agora e vivencie uma #EscolaQueSente com o LIV!</p>
                            </div>
                        </div>

                        <div className="col-12 col-md-6 align-self-center text-center">
                            <div className="btns-acesso-evento">
                                <Link href="/fachada">
                                    <a className="btn btn-plataforma-3d btn-lg">
                                        <strong>ACESSAR A PLATAFORMA</strong>
                                    </a>
                                </Link>
                                {/* <Link href="/register">
                                    <a className="btn btn-plataforma-3d btn-lg">
                                        <strong>CADASTRE-SE</strong> PARA <br />ACESSAR A PLATAFORMA
                                    </a>
                                </Link>
                                <Link href="/login">
                                    <a className="btn btn-faq btn-lg shadow-sm">
                                        <p className="m-0 text-center">OU <strong>ENTRE POR AQUI</strong>, <br /> CASO JÁ TENHA CADASTRO!</p>
                                    </a>
                                </Link> */}
                            </div>
                        </div>
                    </div>
                </div>
            </section> }

            {authenticated && <section id="description-logado" className="shadow-sm">
                <div className="d-flex flex-column flex-md-row">
                    <div className="bg-blue">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <div className="text">
                                        <h2 className="text-uppercase mb-3">QUE BOM TER VOCÊ AQUI!</h2>
                                        <p>Somos o Laboratório Inteligência de Vida, programa que desenvolve o pilar socioemocional nas escolas de todo o Brasil. Criamos espaços de fala e de escuta para ampliar a compreensão que os alunos têm de si, do outro e do mundo.</p>

                                        <p>Antes de tudo, que tal conhecer um pouco da #EscolaQueSente? Ao clicar em "Acesse a plataforma", você conhecerá a fachada da nossa escola. Clicando na porta
                                        de entrada, você é nosso convidado para explorar o lounge, espaço com diferentes conteúdos e experiências divididos em portas coloridas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="bg-yellow">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 align-self-center">
                                    <div className="bloco-acesse-plataforma pt-md-0">
                                        <Link href="/fachada">
                                            <a className="btn btn-plataforma-3d font-weight-bold">
                                                ACESSE A PLATAFORMA
                                            </a>
                                        </Link>

                                        <div className="text">
                                            <p>Esse espaço é seu! Portanto, fique à vontade para clicar nas portas e retornar (usando a seta do seu navegador) a qualquer momento.</p>
                                            <p><strong>Dica:</strong> A porta "Exposição LIV" é o ambiente mais diverso e interativo que você vai encontrar! Nele, poderá conhecer mais detalhes
                                            da experiência LIV, desde a Educação Infantil até o Ensino Médio, incluindo o programa LIV+, a Plataforma de Ensino Eleva e um espaço para conversar com nossos
                                            consultores. As áreas clicáveis estarão sempre piscando na tela. <strong>Completamente imperdível!</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> }

            <section id="banner-home-lobby">
                <div className="container-fluid">
                    <div className="row d-flex justify-content-center">
                        <Link href="/fachada">
                            <a>
                                <img
                                    src="./images/banner-escola-que-sente.jpg"
                                    alt="banner-escola-que-sente"
                                    className="img-fluid" />
                                <div className="col-12 d-flex justify-content-center bloco-banner">
                                    <div className={ `${authenticated ? 'banner-texto-escola-logado' : 'banner-texto-escola'} text-center py-3 col-md-8` }>
                                        <p className="mb-0">VIVENCIE A <strong>#ESCOLAQUESENTE</strong> JUNTO AO PROGRAMA <br /> QUE DÁ VOZ ÀS EMOÇÕES DE ALUNOS PELO BRASIL! </p>
                                    </div>
                                </div>
                            </a>
                        </Link>
                    </div>
                </div>
            </section>

            <section className={ `${authenticated ? 'meet-bg-yellow' : 'meet-bg-blue'} meet` }>
                <div className="container">
                    <div className="form-row">
                        <div className="col-12 col-md-6 pr-md-4">
                            <h2 className="mb-3">CONHEÇA O LIV!</h2>
                            <p>O LIV é o programa socioemocional que está presente em mais de 350 instituições de ensino parceiras com 200 mil alunos e famílias, da Educação Infantil ao Ensino Médio, buscando que os
                            estudantes possam conhecer seus sentimentos e desenvolver habilidades para a vida.
                            </p>
                            <p><strong><a href="https://www.inteligenciadevida.com.br/" target="_blank">CLIQUE AQUI E CONHEÇA MAIS SOBRE O LIV!</a></strong></p>
                        </div>
                        <div className="col-12 col-md-6 align-self-center text-center text-md-right bloco-video-iframe">
                            <div className="bloco-video-iframe-tracos"></div>
                            <iframe width="93%" height="315" src="https://www.youtube.com/embed/2ZS-1WAww-0?mute=1&loop=1" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div>
                    </div>
                </div>
            </section>

            <section className={ `${authenticated ? 'contact-bg-yellow' : 'contact-bg-blue'} contact` } >
                <div className="container">
                    <h2 className="mb-3">DÚVIDAS?</h2>

                    <a href="mailto:contato@inteligenciadevida.com.br" className="btn btn-contato">CONTATO@INTELIGENCIADEVIDA.COM.BR</a>
                </div>
            </section>

            <footer className={ `${authenticated ? 'footer-yellow' : 'footer-blue'} text-center` }>
                <div className="container">
                    <div className="d-flex justify-content-center justify-content-md-start">
                        <a href="https://www.inteligenciadevida.com.br/" target="_blank" className="btn btn-social shadow-sm mx-2"><FaMousePointer size={ 22 } style={ { color: '#fff' } } /></a>
                        <a href={ process.env.LINK_FACEBOOK } target="_blank" className="btn btn-social shadow-sm mx-2"><FaFacebookF size={ 22 } style={ { color: '#fff' } } /></a>
                        <a href={ process.env.LINK_INSTAGRAM } target="_blank" className="btn btn-social shadow-sm mx-2"><FaInstagram size={ 22 } style={ { color: '#fff' } } /></a>
                        <a href={ process.env.LINK_LINKEDIN } target="_blank" className="btn btn-social shadow-sm mx-2"><FaLinkedinIn size={ 22 } style={ { color: '#fff' } } /></a>
                        <a href={ process.env.LINK_YOUTUBE } target="_blank" className="btn btn-social shadow-sm mx-2"><FaYoutube size={ 22 } style={ { color: '#fff' } } /></a>
                    </div>
                </div>
            </footer>
        </>
    )
}