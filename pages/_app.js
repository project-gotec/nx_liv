import Header from '../components/Header';
import React, { useEffect } from 'react';
import {AuthProvider} from '../context/AuthContext';
import 'react-notifications/lib/notifications.css';
import '../styles/styles.scss';

function MyApp({ Component, pageProps }) {
  return (
      <AuthProvider>
        <Header />
        <Component {...pageProps} />
      </AuthProvider>
  )
}

export default MyApp
