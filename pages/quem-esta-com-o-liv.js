import React, { useContext, useEffect } from 'react';

import Head from 'next/head';
import Link from 'next/link';

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function QuemEstaComOLiv() {
    useEffect( () => {
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Quem está com o LIV</title>
            </Head>
            <div className="pages">
                <img src="/images/3d/quem-esta-com-liv/sessao-1.png" className="img-background position-relative" height="4606" width="1920" />
                <img src="/images/3d/quem-esta-com-liv/sessao-2.png" className="img-background position-relative" height="4606" width="1920" />
                <img src="/images/3d/quem-esta-com-liv/sessao-3.png" className="img-background position-relative" height="4606" width="1920" />
                <img src="/images/3d/quem-esta-com-liv/sessao-4.png" className="img-background position-relative" height="4606" width="1920" />
                <img src="/images/3d/quem-esta-com-liv/sessao-5.png" className="img-background position-relative" height="4606" width="1920" />

                <div className="navgation">
                    <Link href="/lobby">
                        <a className="btn btn-back"><FaArrowLeft size={ 22 } /></a>
                    </Link>
                </div>
            </div>
        </>
    )
}