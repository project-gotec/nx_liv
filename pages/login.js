import Head from 'next/head';
import AuthServer from './api/auth';
import React, { useState, useContext, useEffect } from 'react';
import { Context } from "../context/AuthContext";
import { useRouter } from 'next/router';
import Link from 'next/link';
import ReactGA from 'react-ga';

export default function Login() {
    const router = useRouter();
    const {setUser, authenticated, setAuthenticated} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [errors, setErrors] = useState({});

    const validatorMessage = (input) => {
        let data = {
            invalid: false,
            message: ''
        }

        if (errors.hasOwnProperty(input)) {
            data.invalid = true;
            data.message = errors[input][0];
        }

        return data;
    }

    const handleLogin = (event) => {
        event.preventDefault();

        setLoading(true);

        if(email == ''){
            setErrors({
                email: ['Informe seu e-mail.']
            });
            setLoading(false);
            return false;
        }

        if(password == ''){
            setErrors({
                password: ['Informe sua senha.']
            });
            setLoading(false);
            return false;
        }

        AuthServer.login({
            id_evento: process.env.APP_ID_EVENTO,
            email: email,
            password: password
        }).then(({data}) => {
            localStorage.setItem('user', JSON.stringify(data.user));

            setUser(data.user);
            setAuthenticated(true);
            window.location = '/';
        }).catch(({response}) => {
            setError(false);
            setErrorMessage('');
            setErrors({});

            if(response.status == 400 || response.status == 500){
                setError(true);
                setErrorMessage("Houve um problema ao fazer o login, tente novamente em alguns segundos!");

                return false;
            }

            if(!response.data.hasOwnProperty('errors')){
                setError(true);
                setErrorMessage(response.data.message);

                return false;
            }
            
            setErrors(response.data.errors);
        }).finally(() => {
            setLoading(false);
        });
    }

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Login</title>
                <link rel="shortcut icon" href="/images/icons/favicon.png" />
            </Head>
            { 
                !authenticated && <div id="container-auth" className="login-auth">
                    <div id="auth-form" className="d-flex flex-column justify-content-center">
                        <div className="card mt-5 mt-md-0" id="card-auth">
                            <div className="card-header text-center">
                                <h4 className="mb-3">Que bom ter você aqui!</h4>
                                <span>Se você já realizou seu cadastro, entre com seu e-mail e senha.</span>
                            </div>
                            <div className="card-body">
                                <form onSubmit={handleLogin}>
                                    <div className="form-group">
                                        <label htmlFor="email">E-mail</label>
                                        <input 
                                            id="email" 
                                            type="email" 
                                            className={`form-control ${validatorMessage('email').invalid ? 'is-invalid': ''}`} 
                                            value={email} 
                                            aria-describedby="emailFeedback" 
                                            onChange={e => setEmail(e.target.value)} />
                                        {validatorMessage('email').invalid ? <div id="emailFeedback" className="invalid-feedback">{validatorMessage('email').message}</div> : ''}
                                    </div>
                
                                    <div className="form-group">
                                        <div className="d-flex justify-content-between align-items-center">
                                            <label htmlFor="password">Senha</label>
                                            <Link href="/esqueci-minha-senha"><a className="mb-2">Esqueci minha senha</a></Link>
                                        </div>
                                        <input 
                                            type="password" 
                                            className={`form-control ${validatorMessage('password').invalid ? 'is-invalid': ''}`} 
                                            value={password} 
                                            aria-describedby="passwordFeedback"
                                            onChange={e => setPassword(e.target.value)} />
                                        {validatorMessage('password').invalid ? <div id="passwordFeedback" className="invalid-feedback">{validatorMessage('password').message}</div> : ''}
                                    </div>

                                    {error && <div className="alert alert-danger text-center" role="alert">
                                        {errorMessage}
                                    </div>}
                
                                    <div className="form-group d-flex justify-content-center align-items-center mt-4">
                                        <button type="submit" className="btn btn-login" disabled={loading}>
                                            {loading
                                                ? <div className="spinner-border spinner-border-sm mr-2" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                                : ''}
                                            {loading 
                                            ? <span>Entrando</span> 
                                            : <span>Entrar</span>}
                                        </button>
                                        <span className="mx-3">ou</span>
                                        <Link href="/register">
                                            <a className="btn btn-link p-0">Cadastre-se agora</a>
                                        </Link>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="auth-illustration"
                    style={{ backgroundImage: 'url(https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/banner_login.jpg)' }}></div>
                </div>
            }
        </>
    )
}