import React, { useContext, useEffect } from 'react';

import Head from 'next/head';
import Link from 'next/link';

// import { Context } from "../../../context/AuthContext";

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function Fundamental123Ano() {
    // const {authenticated, checkAuthenticated} = useContext(Context);

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Fundamental 1, 2, 3 ano - Video</title>
            </Head>
            <div className="pages">
                <div className="video">
                    <iframe src="https://www.youtube.com/embed/Rh7aCVbkm-U" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen></iframe>
                </div>
                <div className="navgation">
                    <Link href="/exposicao/fundamental-1-2-3-ano">
                        <a className="btn btn-back mt-5"><FaArrowLeft size={ 22 } /></a>
                    </Link>
                </div>
            </div>
        </>
    )
}