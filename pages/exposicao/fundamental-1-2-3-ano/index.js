import React, { useContext, useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';

// import { Context } from "../../../context/AuthContext";

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function Fundamental123Ano() {
    // const { authenticated, checkAuthenticated } = useContext( Context );

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Fundamental 1, 2, 3 ano</title>
            </Head>
            <div className="pages">
                <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                    <Link href="/exposicao/fundamental-1-2-3-ano/video">
                        <a>
                            <polygon id="casa-amarela" className="polygon" points="1096.04,387.94 1096.04,558.62 1087.74,561.81 962.72,562.83 921.7,561.64 
                                918.55,563.26 914.64,564.36 912.6,569.13 801.28,568.87 788.6,557.04 789.28,449.13 799.74,445.47 808.43,441.3 816.43,436.45 
                                825.11,429.47 832.77,421.13 838.55,412.96 842.55,402.4 843.74,391.43 840.51,380.79 834.21,373.81 825.7,369.89 814.3,367.09 
                                799.74,366.06 788.6,366.66 759.57,372.79 750.64,352.45 751.57,344.36 749.53,337.81 742.72,335.09 742.72,333.21 811.74,291.17 
                                872.77,253.81 930.98,217.21 949.79,204.79 987.57,223.43 1008.77,234.4 1006.98,240.79 1004.94,257.04 1004.09,276.7 
                                991.74,279.68 980.43,287.6 974.89,295.6 973.7,306.32 974.55,316.62 978.04,326.49 984.94,336.19 991.66,343.26 1000,348.87 
                                1007.66,352.28 1021.87,352.11 1029.53,348.19 1032.17,352.62 1036.51,357.38 1040.94,359.17 1044.77,366.66 1054.3,376.79 
                                1062.21,382.83 1071.66,386.96 1083.49,389.13 " />
                        </a>
                    </Link>
                    <a href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/fundamental-1-2-3/MINILIVRO1.pdf">
                        <path id="tomas" className="polygon" d="M1567.66,898.57l7.06,3.4l12.09,0.77l9.28-2.98l8-6.47l4.34-7.74v-6.89l-10.55,4.09l-27.57,10.98
                            l4.77-6.55l1.87-8.51l0.43-10.21l-3.91-10.47l-6.47-10.47l-5.62-4.26l6.21-2.89l4.32-2.43l8.62-6.19l7.21-7.28l5.11-8.11l4.21-9.64
                            l2.11-7.15l1.6-9l-0.13-10.21l-2.74-13.09l-4.15-11.17l-4.4-7.72l-4.43-5.74l-6.17-6.51l-1.6-0.89l-1.53-12.38l-3-14.68l-5.36-9.06
                            l-7.15,9.7l-12.89,23.11l-21.35-0.22l-0.29-12.48l-1.15-8.81l-3.51-9.45l-5.55,4.21l-14.36,14.94l-20.3,22.81l-13.45,5.79
                            l-13.28,9.53l-8.85,14.64l-3.74,9.87l-1.19,10.38l1.53,9.19l2.72,5.96l-9.36,7.49l-4.26,5.96l-1.19,14.13l2.38,9.53l7.66,7.15
                            l6.47,3.4l3.74,1.87l-10.04,7.66l-10.21,7.32l1.53,9.19l2.72,12.6l0.51,10.04l22.13,4.77l15.66,3.57l13.45,3.57l5.62,0.85l9.81-6.68
                            l3.96,1.6l-2.23,5.09l-6.77,4.04l-1.79,3.57l1.98,1.79l5.49,0.89l4.15-1.02l4.15-3.57l2.62-5.3l0.13-4.4c0,0,4.79,0,4.98-0.06
                            s3.57-5.74,3.57-5.74l8.81-0.64l-0.06,7.72l6.38,1.4c0,0-9.26,7.72-9.51,8.17c-0.26,0.45,0.06,3.45,0.06,3.45l11.94,0.7l6.96-3.89
                            l2.81-8.36l4.09-0.13c0,0,0.77-9.77,1.09-9.83c0.32-0.06,8.74-3.26,9-3.26C1563.09,902.91,1567.66,898.57,1567.66,898.57z" />
                    </a>
                    <a href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/fundamental-1-2-3/ANOSINICIAIS123LIV.pdf">
                        <polygon id="casa-amarela" className="polygon" points="1706.06,963.74 1706.64,933.87 1712.13,927.11 1725.79,927.11 1726.11,543.94 
                            1730.13,535.3 1738.17,530.32 1812.94,527.09 1888.17,524.28 1928.51,523.09 1936.6,524.7 1941.36,530.23 1944.34,535.34 
                            1943.66,952.11 1954.47,953.72 1962.04,957.21 1964.34,962.06 1964.34,972.11 1964.34,993.64 1961.36,997.81 " />
                    </a>
                </svg>
                <div className="navgation">
                    <Link href="/exposicao">
                        <a className="btn btn-back"><FaArrowLeft size={ 22 } /></a>
                    </Link>
                </div>
                <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/3d/fundamental-anos-iniciais-1-2-3.jpg" className="img-background" height="1062" width="2000" />
            </div>
        </>
    )
}