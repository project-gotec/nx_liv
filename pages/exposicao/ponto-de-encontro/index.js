import React, { useContext, useState, useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';

// import { Context } from '../../../context/AuthContext';

import EventoServer from '../../api/evento';

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function AreaDeNegocios() {
    // const { authenticated, checkAuthenticated } = useContext( Context );
    const [ evento, setEvento ] = useState( null );

    const getEvento = async () => {
        const { data } = await EventoServer.show();
        setEvento( data );
    }

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );

        // getEvento();
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Ponto de encontro</title>
            </Head>
            <div className="pages">
                    <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                        <a href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/ponto-de-encontro/PAINEL.pdf">
                            <polygon className="polygon" points="52.5,643.5 53.5,54.5 236.5,85.5 357.5,103.5 463.5,117.5 554.5,127.5 673.5,137.5 750.5,142.5 
	                                936.5,144.5 937.5,144.5 937.5,144.5 937.5,144.5 937.5,144.5 938.5,144.5 938.5,159.5 938.5,288.5 940.5,401.5 940.5,518.5 
	                                942.5,631.5 942.5,633.5 852.5,634.5 737.5,635.5 581.5,637.5 473.5,638.5 439.5,638.5 377.5,639.5 330.5,640.5 204.5,641.5 "/>
                        </a>
                    </svg>
                    <div className="navgation">
                        <Link href="/exposicao">
                            <a className="btn btn-back"><FaArrowLeft size={ 22 } /></a>
                        </Link>
                    </div>
                    <img src="/images/3d/ponto-de-encontro.jpg" className="img-background" height="1062" width="2000" />
                </div>
        </>
    )
}