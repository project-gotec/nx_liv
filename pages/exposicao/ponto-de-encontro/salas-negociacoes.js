import React, {useContext, useState, useEffect} from 'react';
import { Modal, Button } from 'react-bootstrap';
import Head from 'next/head';
// import {Context} from '../../../context/AuthContext';
import Link from 'next/link';
import ReactGA from 'react-ga';
import { FaArrowLeft } from "react-icons/fa";

export default function SalasNegociacoes() {
    // const {authenticated, checkAuthenticated} = useContext(Context);

    useEffect(() => {
        // checkAuthenticated();
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <Head>
                <title>{process.env.APP_NAME} | Salas de negociações</title>
            </Head>
            <section className="bg-green-liv py-4" style={{height: '100vh'}}>
                    <div className="container">
                        <div className="form-row">
                            <div className="col-12 col-md-2">
                                <Link href="/exposicao/area-de-negocios">
                                    <a className="btn btn-lg d-flex align-items-center mb-3 px-0" style={{color: '#fff'}}>
                                        <FaArrowLeft size={22} />
                                        <span>Voltar</span>
                                    </a>
                                </Link>
                            </div>
                            <div className="col-12 text-center mb-3">
                                <h4>Bem-Vindo(a)!</h4>
                                <p className="m-0" style={{ fontSize: '1rem' }}>Aqui você pode conversar com exclusividade com um dos nossos Consultores LIV.</p>
                                <p className="m-0" style={{ fontSize: '1rem' }}>É muito fácil! Escolha uma sala com a indicação <strong>“LIVRE”</strong> e clique em <strong>Acessar</strong>.</p>
                                <p className="m-0" style={{ fontSize: '1rem' }}>Pronto, você será recebido por um Consultor LIV e poderá tirar suas dúvidas.</p>
                            </div>
                        </div>
                    </div>
                </section>
        </>
    )
}