import React, { useContext, useEffect } from 'react';

import Head from 'next/head';
import Link from 'next/link';

// import { Context } from "../../../context/AuthContext";

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function FundamentalAnosFinais() {
    // const { authenticated, checkAuthenticated } = useContext( Context );

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Fundamental Anos Finais</title>
            </Head>
             <div className="pages">
                    <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                        <Link href="/exposicao/fundamental-anos-finais/video">
                            <a>
                                <path id="cinema" className="polygon" d="M1247.64,746.66l156.11-571.32l-37.83-13.62l-20.17-8.04l-20.81-5.36l-15.32-2.3h-16.47
                                l-22.17,3.7l-23.34,8.17l-21.6,10.21l-50.72,30.3l-26.55,13.28l-23.83,8l-17.87,2.72l-21.79-0.17l-27.23-6.98l-52.94-26.04
                                L984.34,179l-22.81-6.3l-18.89-2.72l-28.43,2.21l-34.55,12.43l-40.51,19.4l-33.02,13.62l-22.98,3.57h-16.51l-18.04-2.89
                                c0,0-17.53-5.11-18.38-5.45s-17.87-8.85-18.89-9.19s-28.43-17.53-28.43-17.53l-27.06-16.17l-21.45-11.23l-19.91-6.3l-22.98-3.91
                                l-24-0.34l-20.77,3.23l-42.38,12.77l-29.96,9.02L586.3,565.3l8.09-0.09l4.85,2.89l2.55,3.83l-0.6,47.06l16.17,53.87l54.98-0.68
                                l3.32,2.72l2.89,3.83l-0.34,75.66l8.34-0.77l-0.43-73.45l2.13-6.3l7.06-4.09l91.32-1.45l11.74,5.36l10.3,6.55l1.53,6.04l-0.09,62.72
                                l365.51,3.81l2.62-0.83l0.06-7.85L1247.64,746.66z" />
                            </a>
                        </Link>
                        <a href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/anos-finais/ANOSFINAISLIV.pdf">
                            <polygon id="totem" className="polygon" points="0.47,856.83 7.87,861.04 183.32,837.04 186.72,835.34 186.72,812.02 182.3,808.79 
                            177.19,807.26 171.06,807.26 170.67,525.14 167.67,520.61 162.6,517 154.3,516.52 28.04,516.52 21.74,519.72 17.4,526.23 16.68,827 
                            0.47,829.21 " />
                        </a>
                        <a href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/anos-finais/JOGOS.pdf">
                            <path id="jogos" className="polygon" d="M1247.7,747.17l156.96-571.21c0,0,28.79,7.15,28.98,7.34c0.19,0.19,18.45-0.45,18.45-0.45
                            l20.36-2.17l27.51-9.64l23.11-13.53l22.21-15.7l40.34-31.91l23.36-16.6l25.53-13.02l21.83-7.4l29.36-7.53l17.87-0.64l44.55,4.6
                            l46.47,6.26l52.85,5.49l39.57,0.89l28.09-5.62l19.79-4.98l42.13-18.64L2000,34.32v813.19l-23.83-4.68l0.09,10.55l-159.83-16.77
                            l0.32-24.21l-15.32-2.23l-4.91,7.4l-147.45-14.74l0.64-16.21l-6-0.77l-16.21,8.68c0,0-128.11-11.17-127.91-12.77
                            c0.19-1.6,0.38-13.66,0.38-13.66h-7.85l-26.62,12l-111.11-11.74l0.34-13.96L1247.7,747.17z" />
                        </a>
                    </svg>
                    <div className="navgation">
                        <Link href="/exposicao">
                            <a className="btn btn-back"><FaArrowLeft size={ 22 } /></a>
                        </Link>
                    </div>
                    <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/3d/fundamental-anos-finais.jpg" className="img-background" height="1062" width="2000" />
                </div>
        </>
    )
}