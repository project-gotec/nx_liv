import React, { useContext, useEffect } from 'react';

import Head from 'next/head';
import Link from 'next/link';

// import { Context } from "../../../context/AuthContext";

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function TrailerLiv() {
    // const { authenticated, checkAuthenticated } = useContext( Context );

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | LIV+ Trailer</title>
            </Head>
            <div class="pages">
                <div class="video">
                    <iframe src="https://www.youtube.com/embed/CxYGBh5T0M4" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen></iframe>
                </div>
                <div class="navgation">
                    <Link href="/exposicao/liv-mais">
                        <a class="btn btn-back mt-5"><FaArrowLeft size={ 22 } /></a>
                    </Link>
                </div>
            </div>
        </>
    )
}