import React, { useContext, useEffect } from 'react';

import Head from 'next/head';
import Link from 'next/link';

// import { Context } from "../../../context/AuthContext";

import ReactGA from 'react-ga';

import { FaArrowLeft } from "react-icons/fa";

export default function PlataformaEleva() {
    // const { authenticated, checkAuthenticated } = useContext( Context );

    useEffect( () => {
        // checkAuthenticated();
        ReactGA.pageview( window.location.pathname + window.location.search );
    }, [] );

    return (
        <>
            <Head>
                <title>{ process.env.APP_NAME } | Plataforma eleva</title>
            </Head>
            <div className="pages">
                    <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
                        <a href="http://rockinteractive.postclickmarketing.com/quiz/escolas-preparadas-para-ferramentas-online?">
                            <polygon id="testeira" className="polygon" points="535.49,420.87 1262.64,93.55 1387.49,420.87 " />
                        </a>
                        <a href="https://materiais.elevaplataforma.com.br/">
                            <polygon id="material-didatico" className="polygon" points="535.49,457.64 646.13,720.28 745.19,715 782.98,720.28 743.66,786.49 1520.26,786.23 1397.45,457.64 " />
                        </a>
                        <Link href="/exposicao/plataforma-eleva/video">
                            <a>
                                <polygon id="totem" className="polygon" points="13.11,928.7 13.11,902.15 15.53,898.57 34.17,896.02 33.66,528.23 37.11,521.85 
                            45.79,517.51 201.15,521.6 212.38,523.38 219.53,528.87 221.19,535.38 220.68,813.43 217.23,817.13 221.19,821.34 221.19,873.3 
                            228.47,872.79 240.09,877.89 240.09,904.19 30.72,934.06 20.38,934.06 " />
                            </a>
                        </Link>
                    </svg>
                    <img src="https://sejaliv.s3.us-east-1.amazonaws.com/images/3d/plataforma-eleva.jpg" className="img-background" height="1062" width="2000" />
                    <div className="navgation">
                        <Link href="/exposicao">
                            <a className="btn btn-back"><FaArrowLeft size={ 22 } /></a>
                        </Link>
                    </div>
                </div>
        </>
    )
}