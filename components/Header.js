import Link from 'next/link';
import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import {Context} from '../context/AuthContext';
import { Navbar, Nav, Button } from 'react-bootstrap';

export default function Header() {
    const router = useRouter();
    // const {user, setUser, authenticated, setAuthenticated} = useContext(Context);

    // function logout(){
    //     localStorage.removeItem('user');
    //     localStorage.removeItem('access_token');
        
    //     setAuthenticated(false);
    //     setUser(null);
    //     router.push('/login');
    // }

    function CheckAuthenticated(){
        return (
            <>
                <Nav.Link href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/faq/FAQ.pdf" className="text-center active">FAQ - TIRE SUAS DÚVIDAS</Nav.Link>
                <Nav.Link href="https://www.inteligenciadevida.com.br/pt/queremosliv/seja-uma-escola-parceira/#formulario" target="_blank" className="text-center active">TENHA LIV NA SUA ESCOLA</Nav.Link>
            </>
        );
        // if(!authenticated){
        //     return (
        //         <>
        //             <Nav.Link href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/faq/FAQ.pdf" className="text-center active">FAQ - TIRE SUAS DÚVIDAS</Nav.Link>
        //             <Nav.Link href="https://www.inteligenciadevida.com.br/pt/queremosliv/seja-uma-escola-parceira/#formulario" target="_blank" className="text-center active">TENHA LIV NA SUA ESCOLA</Nav.Link>
        //             <Nav.Link href="/login" className="text-center">Entrar</Nav.Link>
        //         </>
        //     );
        // }else{
        //     return (
        //         <>
        //             <Nav.Link href="https://sejaliv.s3.us-east-1.amazonaws.com/images/v2/pdf/faq/FAQ.pdf" className="text-center active">FAQ - TIRE SUAS DÚVIDAS</Nav.Link>
        //             <Nav.Link href="">{user.name}</Nav.Link>
        //             <Button variant="outline-danger" onClick={logout}>Sair</Button>
        //         </>
        //     );
        // }   
    }

    return (
        <>
            <Navbar bg="light" expand="lg" className="navbar-liv shadow-sm">
                <Navbar.Brand href="/">
                    <img src="/images/logo.png" className="img-fluid" style={{width: 95}} />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <CheckAuthenticated />
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}