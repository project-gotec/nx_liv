const path = require('path')

module.exports = {
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')],
    },
    trailingSlash: true,
    env: {
        APP_NAME: process.env.APP_NAME,
        APP_API: process.env.APP_API,
        APP_RECAPTCHA_GOOGLE: process.env.APP_RECAPTCHA_GOOGLE,
        LINK_FACEBOOK: process.env.LINK_FACEBOOK,
        LINK_INSTAGRAM: process.env.LINK_INSTAGRAM,
        LINK_LINKEDIN: process.env.LINK_LINKEDIN,
        LINK_YOUTUBE: process.env.LINK_YOUTUBE
    }
}