webpackHotUpdate_N_E("pages/login",{

/***/ "./context/AuthContext.js":
/*!********************************!*\
  !*** ./context/AuthContext.js ***!
  \********************************/
/*! exports provided: Context, AuthProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return Context; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthProvider", function() { return AuthProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-ga */ "./node_modules/react-ga/dist/esm/index.js");
var _jsxFileName = "C:\\git\\nx_liv\\context\\AuthContext.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



react_ga__WEBPACK_IMPORTED_MODULE_2__["default"].initialize('UA-98919187-4');
var Context = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();

function AuthProvider(_ref) {
  _s();

  var children = _ref.children;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"])();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      user = _useState[0],
      setUser = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      authenticated = _useState2[0],
      setAuthenticated = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      token = _useState3[0],
      setToken = _useState3[1];

  var checkAuthenticated = function checkAuthenticated() {
    var userSession = JSON.parse(localStorage.getItem('user')); // if(userSession){
    //     setAuthenticated(true);
    //     setUser(userSession);
    //     if(router.route != '/' && router.route != '/validar-email'){
    //         if(userSession.email_verified_at == null || userSession.email_verified_at == ''){
    //             router.push('/verificar-email');
    //         }
    //     }
    //     if(router.route == '/login' || router.route == '/register'){
    //         router.push('/');
    //     }
    // }else{
    //     const urls = ['/', '/register', '/login', '/resetar-senha'];
    //     setAuthenticated(false);
    //     setUser(null);
    //     if(!urls.includes(router.pathname)) router.push('/login');
    // }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    checkAuthenticated();
  }, []);
  return __jsx(Context.Provider, {
    value: {
      user: user,
      setUser: setUser,
      authenticated: authenticated,
      setAuthenticated: setAuthenticated,
      checkAuthenticated: checkAuthenticated
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 9
    }
  }, children);
}

_s(AuthProvider, "BGHxsBJBXZFVmLbg0BZnaLheESM=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"]];
});

_c = AuthProvider;


var _c;

$RefreshReg$(_c, "AuthProvider");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29udGV4dC9BdXRoQ29udGV4dC5qcyJdLCJuYW1lcyI6WyJSZWFjdEdBIiwiaW5pdGlhbGl6ZSIsIkNvbnRleHQiLCJjcmVhdGVDb250ZXh0IiwiQXV0aFByb3ZpZGVyIiwiY2hpbGRyZW4iLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ1c2VTdGF0ZSIsInVzZXIiLCJzZXRVc2VyIiwiYXV0aGVudGljYXRlZCIsInNldEF1dGhlbnRpY2F0ZWQiLCJ0b2tlbiIsInNldFRva2VuIiwiY2hlY2tBdXRoZW50aWNhdGVkIiwidXNlclNlc3Npb24iLCJKU09OIiwicGFyc2UiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwidXNlRWZmZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUFBLGdEQUFPLENBQUNDLFVBQVIsQ0FBbUIsZUFBbkI7QUFFQSxJQUFNQyxPQUFPLGdCQUFHQywyREFBYSxFQUE3Qjs7QUFFQSxTQUFTQyxZQUFULE9BQWlDO0FBQUE7O0FBQUEsTUFBVkMsUUFBVSxRQUFWQSxRQUFVO0FBQzdCLE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7O0FBRDZCLGtCQUVMQyxzREFBUSxDQUFDLElBQUQsQ0FGSDtBQUFBLE1BRXRCQyxJQUZzQjtBQUFBLE1BRWhCQyxPQUZnQjs7QUFBQSxtQkFHYUYsc0RBQVEsQ0FBQyxLQUFELENBSHJCO0FBQUEsTUFHdEJHLGFBSHNCO0FBQUEsTUFHUEMsZ0JBSE87O0FBQUEsbUJBSUhKLHNEQUFRLENBQUMsSUFBRCxDQUpMO0FBQUEsTUFJdEJLLEtBSnNCO0FBQUEsTUFJZkMsUUFKZTs7QUFNN0IsTUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixHQUFNO0FBQzdCLFFBQU1DLFdBQVcsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixNQUFyQixDQUFYLENBQXBCLENBRDZCLENBRzdCO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDSCxHQXpCRDs7QUEyQkFDLHlEQUFTLENBQUMsWUFBTTtBQUNaTixzQkFBa0I7QUFDckIsR0FGUSxFQUVOLEVBRk0sQ0FBVDtBQUlBLFNBQ0ksTUFBQyxPQUFELENBQVMsUUFBVDtBQUFrQixTQUFLLEVBQUU7QUFDckJOLFVBQUksRUFBSkEsSUFEcUI7QUFFckJDLGFBQU8sRUFBUEEsT0FGcUI7QUFHckJDLG1CQUFhLEVBQWJBLGFBSHFCO0FBSXJCQyxzQkFBZ0IsRUFBaEJBLGdCQUpxQjtBQUtyQkcsd0JBQWtCLEVBQWxCQTtBQUxxQixLQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBT0tWLFFBUEwsQ0FESjtBQVdIOztHQWhEUUQsWTtVQUNVRyxxRDs7O0tBRFZILFk7QUFrRFQiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvbG9naW4uNzQ2MDFhYTVhYzc2ZTBhMTg2YWMuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwge2NyZWF0ZUNvbnRleHQsIHVzZVN0YXRlLCB1c2VFZmZlY3R9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5pbXBvcnQgUmVhY3RHQSBmcm9tICdyZWFjdC1nYSc7XHJcblxyXG5SZWFjdEdBLmluaXRpYWxpemUoJ1VBLTk4OTE5MTg3LTQnKTtcclxuXHJcbmNvbnN0IENvbnRleHQgPSBjcmVhdGVDb250ZXh0KCk7XHJcblxyXG5mdW5jdGlvbiBBdXRoUHJvdmlkZXIoe2NoaWxkcmVufSl7XHJcbiAgICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuICAgIGNvbnN0IFt1c2VyLCBzZXRVc2VyXSA9IHVzZVN0YXRlKG51bGwpO1xyXG4gICAgY29uc3QgW2F1dGhlbnRpY2F0ZWQsIHNldEF1dGhlbnRpY2F0ZWRdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gICAgY29uc3QgW3Rva2VuLCBzZXRUb2tlbl0gPSB1c2VTdGF0ZShudWxsKTtcclxuXHJcbiAgICBjb25zdCBjaGVja0F1dGhlbnRpY2F0ZWQgPSAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgdXNlclNlc3Npb24gPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyJykpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vIGlmKHVzZXJTZXNzaW9uKXtcclxuICAgICAgICAvLyAgICAgc2V0QXV0aGVudGljYXRlZCh0cnVlKTtcclxuICAgICAgICAvLyAgICAgc2V0VXNlcih1c2VyU2Vzc2lvbik7XHJcblxyXG4gICAgICAgIC8vICAgICBpZihyb3V0ZXIucm91dGUgIT0gJy8nICYmIHJvdXRlci5yb3V0ZSAhPSAnL3ZhbGlkYXItZW1haWwnKXtcclxuICAgICAgICAvLyAgICAgICAgIGlmKHVzZXJTZXNzaW9uLmVtYWlsX3ZlcmlmaWVkX2F0ID09IG51bGwgfHwgdXNlclNlc3Npb24uZW1haWxfdmVyaWZpZWRfYXQgPT0gJycpe1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIHJvdXRlci5wdXNoKCcvdmVyaWZpY2FyLWVtYWlsJyk7XHJcbiAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgLy8gICAgIGlmKHJvdXRlci5yb3V0ZSA9PSAnL2xvZ2luJyB8fCByb3V0ZXIucm91dGUgPT0gJy9yZWdpc3Rlcicpe1xyXG4gICAgICAgIC8vICAgICAgICAgcm91dGVyLnB1c2goJy8nKTtcclxuICAgICAgICAvLyAgICAgfVxyXG5cclxuICAgICAgICAvLyB9ZWxzZXtcclxuICAgICAgICAvLyAgICAgY29uc3QgdXJscyA9IFsnLycsICcvcmVnaXN0ZXInLCAnL2xvZ2luJywgJy9yZXNldGFyLXNlbmhhJ107XHJcblxyXG4gICAgICAgIC8vICAgICBzZXRBdXRoZW50aWNhdGVkKGZhbHNlKTtcclxuICAgICAgICAvLyAgICAgc2V0VXNlcihudWxsKTtcclxuXHJcbiAgICAgICAgLy8gICAgIGlmKCF1cmxzLmluY2x1ZGVzKHJvdXRlci5wYXRobmFtZSkpIHJvdXRlci5wdXNoKCcvbG9naW4nKTtcclxuICAgICAgICAvLyB9XHJcbiAgICB9XHJcblxyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBjaGVja0F1dGhlbnRpY2F0ZWQoKTtcclxuICAgIH0sIFtdKTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXt7XHJcbiAgICAgICAgICAgIHVzZXIsXHJcbiAgICAgICAgICAgIHNldFVzZXIsXHJcbiAgICAgICAgICAgIGF1dGhlbnRpY2F0ZWQsXHJcbiAgICAgICAgICAgIHNldEF1dGhlbnRpY2F0ZWQsXHJcbiAgICAgICAgICAgIGNoZWNrQXV0aGVudGljYXRlZFxyXG4gICAgICAgIH19PlxyXG4gICAgICAgICAgICB7Y2hpbGRyZW59XHJcbiAgICAgICAgPC9Db250ZXh0LlByb3ZpZGVyPlxyXG4gICAgKTtcclxufVxyXG5cclxuZXhwb3J0IHtDb250ZXh0LCBBdXRoUHJvdmlkZXJ9OyJdLCJzb3VyY2VSb290IjoiIn0=