webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_AuthContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../context/AuthContext */ "./context/AuthContext.js");
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-ga */ "./node_modules/react-ga/dist/esm/index.js");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-icons/fa */ "./node_modules/react-icons/fa/index.esm.js");
var _jsxFileName = "C:\\git\\nx_liv\\pages\\index.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






function Home() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_AuthContext__WEBPACK_IMPORTED_MODULE_3__["Context"]),
      authenticated = _useContext.authenticated;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    react_ga__WEBPACK_IMPORTED_MODULE_4__["default"].pageview(window.location.pathname + window.location.search);
  }, []);
  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 13
    }
  }, __jsx("title", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 17
    }
  }, "SejaLIV")), __jsx("section", {
    id: "banner",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 13
    }
  }, authenticated && __jsx("img", {
    src: "./images/banner-logado.jpg",
    className: "img-fluid",
    alt: "Banner logado",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 36
    }
  }), !authenticated && __jsx("img", {
    src: "./images/banner.jpg",
    className: "img-fluid",
    alt: "Banner",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 37
    }
  })), !authenticated && __jsx("section", {
    id: "description",
    className: "shadow-sm",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 32
    }
  }, __jsx("div", {
    className: "container",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 17
    }
  }, __jsx("div", {
    className: "form-row",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 21
    }
  }, __jsx("div", {
    className: "col-12 col-md-6",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 25
    }
  }, __jsx("div", {
    id: "text",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 29
    }
  }, __jsx("h2", {
    className: "text-uppercase mb-3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 33
    }
  }, "BEM-VINDO(A)!"), __jsx("p", {
    className: "mb-0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 33
    }
  }, "Somos o Laborat\xF3rio Intelig\xEAncia de Vida, programa que desenvolve o pilar socioemocional nas escolas de todo o Brasil. Criamos espa\xE7os de fala e de escuta para ampliar a compreens\xE3o que os alunos t\xEAm de si, do outro e do mundo."), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 33
    }
  }, "Essa \xE9 a plataforma 3D \"Seja LIV\", nossa experi\xEAncia inovadora e exclusiva. Aqui, a comunidade escolar se re\xFAne e vivencia os curr\xEDculos do nosso programa socioemocional. Voc\xEA pode navegar junto com os personagens, visitar a casa amarela, ler as hist\xF3rias do Tom\xE1s, conhecer nossos jogos e seriados e muito mais!"), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 33
    }
  }, "Acesse agora e vivencie uma #EscolaQueSente com o LIV!"))), __jsx("div", {
    className: "col-12 col-md-6 align-self-center text-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 25
    }
  }, __jsx("div", {
    className: "btns-acesso-evento",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 29
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/fachada",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 33
    }
  }, __jsx("a", {
    className: "btn btn-plataforma-3d btn-lg",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 37
    }
  }, __jsx("strong", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 41
    }
  }, "ACESSAR A PLATAFORMA")))))))), authenticated && __jsx("section", {
    id: "description-logado",
    className: "shadow-sm",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 31
    }
  }, __jsx("div", {
    className: "d-flex flex-column flex-md-row",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 17
    }
  }, __jsx("div", {
    className: "bg-blue",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 21
    }
  }, __jsx("div", {
    className: "container",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 25
    }
  }, __jsx("div", {
    className: "row",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 29
    }
  }, __jsx("div", {
    className: "col-12",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 33
    }
  }, __jsx("div", {
    className: "text",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 37
    }
  }, __jsx("h2", {
    className: "text-uppercase mb-3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 41
    }
  }, "QUE BOM TER VOC\xCA AQUI!"), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 41
    }
  }, "Somos o Laborat\xF3rio Intelig\xEAncia de Vida, programa que desenvolve o pilar socioemocional nas escolas de todo o Brasil. Criamos espa\xE7os de fala e de escuta para ampliar a compreens\xE3o que os alunos t\xEAm de si, do outro e do mundo."), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 41
    }
  }, "Antes de tudo, que tal conhecer um pouco da #EscolaQueSente? Ao clicar em \"Acesse a plataforma\", voc\xEA conhecer\xE1 a fachada da nossa escola. Clicando na porta de entrada, voc\xEA \xE9 nosso convidado para explorar o lounge, espa\xE7o com diferentes conte\xFAdos e experi\xEAncias divididos em portas coloridas.")))))), __jsx("div", {
    className: "bg-yellow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 21
    }
  }, __jsx("div", {
    className: "container",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 25
    }
  }, __jsx("div", {
    className: "row",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 29
    }
  }, __jsx("div", {
    className: "col-12 align-self-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 33
    }
  }, __jsx("div", {
    className: "bloco-acesse-plataforma pt-md-0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 37
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/fachada",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 41
    }
  }, __jsx("a", {
    className: "btn btn-plataforma-3d font-weight-bold",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90,
      columnNumber: 45
    }
  }, "ACESSE A PLATAFORMA")), __jsx("div", {
    className: "text",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 41
    }
  }, __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96,
      columnNumber: 45
    }
  }, "Esse espa\xE7o \xE9 seu! Portanto, fique \xE0 vontade para clicar nas portas e retornar (usando a seta do seu navegador) a qualquer momento."), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 45
    }
  }, __jsx("strong", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 48
    }
  }, "Dica:"), " A porta \"Exposi\xE7\xE3o LIV\" \xE9 o ambiente mais diverso e interativo que voc\xEA vai encontrar! Nele, poder\xE1 conhecer mais detalhes da experi\xEAncia LIV, desde a Educa\xE7\xE3o Infantil at\xE9 o Ensino M\xE9dio, incluindo o programa LIV+, a Plataforma de Ensino Eleva e um espa\xE7o para conversar com nossos consultores. As \xE1reas clic\xE1veis estar\xE3o sempre piscando na tela. ", __jsx("strong", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 110
    }
  }, "Completamente imperd\xEDvel!")))))))))), __jsx("section", {
    id: "banner-home-lobby",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "container-fluid",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110,
      columnNumber: 17
    }
  }, __jsx("div", {
    className: "row d-flex justify-content-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111,
      columnNumber: 21
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/fachada",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 25
    }
  }, __jsx("a", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 29
    }
  }, __jsx("img", {
    src: "./images/banner-escola-que-sente.jpg",
    alt: "banner-escola-que-sente",
    className: "img-fluid",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114,
      columnNumber: 33
    }
  }), __jsx("div", {
    className: "col-12 d-flex justify-content-center bloco-banner",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 33
    }
  }, __jsx("div", {
    className: "".concat(authenticated ? 'banner-texto-escola-logado' : 'banner-texto-escola', " text-center py-3 col-md-8"),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119,
      columnNumber: 37
    }
  }, __jsx("p", {
    className: "mb-0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 41
    }
  }, "VIVENCIE A ", __jsx("strong", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 72
    }
  }, "#ESCOLAQUESENTE"), " JUNTO AO PROGRAMA ", __jsx("br", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 123
    }
  }), " QUE D\xC1 VOZ \xC0S EMO\xC7\xD5ES DE ALUNOS PELO BRASIL! ")))))))), __jsx("section", {
    className: "".concat(authenticated ? 'meet-bg-yellow' : 'meet-bg-blue', " meet"),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 129,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "container",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 130,
      columnNumber: 17
    }
  }, __jsx("div", {
    className: "form-row",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 131,
      columnNumber: 21
    }
  }, __jsx("div", {
    className: "col-12 col-md-6 pr-md-4",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 132,
      columnNumber: 25
    }
  }, __jsx("h2", {
    className: "mb-3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 133,
      columnNumber: 29
    }
  }, "CONHE\xC7A O LIV!"), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 134,
      columnNumber: 29
    }
  }, "O LIV \xE9 o programa socioemocional que est\xE1 presente em mais de 350 institui\xE7\xF5es de ensino parceiras com 200 mil alunos e fam\xEDlias, da Educa\xE7\xE3o Infantil ao Ensino M\xE9dio, buscando que os estudantes possam conhecer seus sentimentos e desenvolver habilidades para a vida."), __jsx("p", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 29
    }
  }, __jsx("strong", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 32
    }
  }, __jsx("a", {
    href: "https://www.inteligenciadevida.com.br/",
    target: "_blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 40
    }
  }, "CLIQUE AQUI E CONHE\xC7A MAIS SOBRE O LIV!")))), __jsx("div", {
    className: "col-12 col-md-6 align-self-center text-center text-md-right bloco-video-iframe",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 139,
      columnNumber: 25
    }
  }, __jsx("div", {
    className: "bloco-video-iframe-tracos",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 140,
      columnNumber: 29
    }
  }), __jsx("iframe", {
    width: "93%",
    height: "315",
    src: "https://www.youtube.com/embed/2ZS-1WAww-0?mute=1&loop=1",
    frameBorder: "0",
    allow: "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
    allowFullScreen: true,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 141,
      columnNumber: 29
    }
  }))))), __jsx("section", {
    className: "".concat(authenticated ? 'contact-bg-yellow' : 'contact-bg-blue', " contact"),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "container",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 148,
      columnNumber: 17
    }
  }, __jsx("h2", {
    className: "mb-3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 149,
      columnNumber: 21
    }
  }, "D\xDAVIDAS?"), __jsx("a", {
    href: "mailto:contato@inteligenciadevida.com.br",
    className: "btn btn-contato",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 151,
      columnNumber: 21
    }
  }, "CONTATO@INTELIGENCIADEVIDA.COM.BR"))), __jsx("footer", {
    className: "".concat(authenticated ? 'footer-yellow' : 'footer-blue', " text-center"),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "container",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 156,
      columnNumber: 17
    }
  }, __jsx("div", {
    className: "d-flex justify-content-center justify-content-md-start",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 157,
      columnNumber: 21
    }
  }, __jsx("a", {
    href: "https://www.inteligenciadevida.com.br/",
    target: "_blank",
    className: "btn btn-social shadow-sm mx-2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 158,
      columnNumber: 25
    }
  }, __jsx(react_icons_fa__WEBPACK_IMPORTED_MODULE_5__["FaMousePointer"], {
    size: 22,
    style: {
      color: '#fff'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 158,
      columnNumber: 132
    }
  })), __jsx("a", {
    href: "https://www.facebook.com/laboratoriointeligenciadevida/",
    target: "_blank",
    className: "btn btn-social shadow-sm mx-2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 159,
      columnNumber: 25
    }
  }, __jsx(react_icons_fa__WEBPACK_IMPORTED_MODULE_5__["FaFacebookF"], {
    size: 22,
    style: {
      color: '#fff'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 159,
      columnNumber: 121
    }
  })), __jsx("a", {
    href: "https://www.instagram.com/laboratoriointeligenciadevida/",
    target: "_blank",
    className: "btn btn-social shadow-sm mx-2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 25
    }
  }, __jsx(react_icons_fa__WEBPACK_IMPORTED_MODULE_5__["FaInstagram"], {
    size: 22,
    style: {
      color: '#fff'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 122
    }
  })), __jsx("a", {
    href: "https://www.linkedin.com/company/inteligenciadevida/",
    target: "_blank",
    className: "btn btn-social shadow-sm mx-2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 161,
      columnNumber: 25
    }
  }, __jsx(react_icons_fa__WEBPACK_IMPORTED_MODULE_5__["FaLinkedinIn"], {
    size: 22,
    style: {
      color: '#fff'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 161,
      columnNumber: 121
    }
  })), __jsx("a", {
    href: "https://www.youtube.com/channel/UC9KKf5hAMcYjgAc0V3CVE9w/",
    target: "_blank",
    className: "btn btn-social shadow-sm mx-2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 162,
      columnNumber: 25
    }
  }, __jsx(react_icons_fa__WEBPACK_IMPORTED_MODULE_5__["FaYoutube"], {
    size: 22,
    style: {
      color: '#fff'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 162,
      columnNumber: 120
    }
  }))))));
}

_s(Home, "4Xw2q+qBhVOrUPu45sRPO1AS/yc=");

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSG9tZSIsInVzZUNvbnRleHQiLCJDb250ZXh0IiwiYXV0aGVudGljYXRlZCIsInVzZUVmZmVjdCIsIlJlYWN0R0EiLCJwYWdldmlldyIsIndpbmRvdyIsImxvY2F0aW9uIiwicGF0aG5hbWUiLCJzZWFyY2giLCJwcm9jZXNzIiwiY29sb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFZSxTQUFTQSxJQUFULEdBQWdCO0FBQUE7O0FBQUEsb0JBQ0RDLHdEQUFVLENBQUVDLDREQUFGLENBRFQ7QUFBQSxNQUNuQkMsYUFEbUIsZUFDbkJBLGFBRG1COztBQUczQkMseURBQVMsQ0FBRSxZQUFNO0FBQ2JDLG9EQUFPLENBQUNDLFFBQVIsQ0FBa0JDLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsUUFBaEIsR0FBMkJGLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkUsTUFBN0Q7QUFDSCxHQUZRLEVBRU4sRUFGTSxDQUFUO0FBSUEsU0FDSSxtRUFDSSxNQUFDLGdEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQVNDLFNBQVQsQ0FESixDQURKLEVBS0k7QUFBUyxNQUFFLEVBQUMsUUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ01SLGFBQWEsSUFBSTtBQUFLLE9BQUcsRUFBQyw0QkFBVDtBQUFzQyxhQUFTLEVBQUMsV0FBaEQ7QUFBNEQsT0FBRyxFQUFDLGVBQWhFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFEdkIsRUFFTSxDQUFDQSxhQUFELElBQWtCO0FBQUssT0FBRyxFQUFDLHFCQUFUO0FBQStCLGFBQVMsRUFBQyxXQUF6QztBQUFxRCxPQUFHLEVBQUMsUUFBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUZ4QixDQUxKLEVBVUssQ0FBQ0EsYUFBRCxJQUFrQjtBQUFTLE1BQUUsRUFBQyxhQUFaO0FBQTBCLGFBQVMsRUFBQyxXQUFwQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ2Y7QUFBSyxhQUFTLEVBQUMsV0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLEVBQUMsaUJBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUssTUFBRSxFQUFDLE1BQVI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUksYUFBUyxFQUFDLHFCQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREosRUFFSTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMFBBRkosRUFJSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVWQUpKLEVBT0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4REFQSixDQURKLENBREosRUFhSTtBQUFLLGFBQVMsRUFBQywrQ0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLEVBQUMsb0JBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJLE1BQUMsZ0RBQUQ7QUFBTSxRQUFJLEVBQUMsVUFBWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBRyxhQUFTLEVBQUMsOEJBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNEJBREosQ0FESixDQURKLENBREosQ0FiSixDQURKLENBRGUsQ0FWdkIsRUFnREtBLGFBQWEsSUFBSTtBQUFTLE1BQUUsRUFBQyxvQkFBWjtBQUFpQyxhQUFTLEVBQUMsV0FBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNkO0FBQUssYUFBUyxFQUFDLGdDQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxTQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxXQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxLQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxRQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxNQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFJLGFBQVMsRUFBQyxxQkFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlDQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwUEFGSixFQUlJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb1VBSkosQ0FESixDQURKLENBREosQ0FESixDQURKLEVBaUJJO0FBQUssYUFBUyxFQUFDLFdBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUssYUFBUyxFQUFDLFdBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUssYUFBUyxFQUFDLEtBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUssYUFBUyxFQUFDLDBCQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxpQ0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0ksTUFBQyxnREFBRDtBQUFNLFFBQUksRUFBQyxVQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFHLGFBQVMsRUFBQyx3Q0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQURKLENBREosRUFPSTtBQUFLLGFBQVMsRUFBQyxNQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9KQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFBSCwrWUFFaUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQ0FGakUsQ0FGSixDQVBKLENBREosQ0FESixDQURKLENBREosQ0FqQkosQ0FEYyxDQWhEdEIsRUEyRkk7QUFBUyxNQUFFLEVBQUMsbUJBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUssYUFBUyxFQUFDLGlCQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxtQ0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0ksTUFBQyxnREFBRDtBQUFNLFFBQUksRUFBQyxVQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFDSSxPQUFHLEVBQUMsc0NBRFI7QUFFSSxPQUFHLEVBQUMseUJBRlI7QUFHSSxhQUFTLEVBQUMsV0FIZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBREosRUFLSTtBQUFLLGFBQVMsRUFBQyxtREFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLFlBQU1BLGFBQWEsR0FBRyw0QkFBSCxHQUFrQyxxQkFBckQsK0JBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUcsYUFBUyxFQUFDLE1BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFBK0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFBL0IseUJBQWtGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFBbEYsK0RBREosQ0FESixDQUxKLENBREosQ0FESixDQURKLENBREosQ0EzRkosRUErR0k7QUFBUyxhQUFTLFlBQU1BLGFBQWEsR0FBRyxnQkFBSCxHQUFzQixjQUF6QyxVQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLEVBQUMsV0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBSyxhQUFTLEVBQUMseUJBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUksYUFBUyxFQUFDLE1BQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMlNBRkosRUFLSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFRO0FBQUcsUUFBSSxFQUFDLHdDQUFSO0FBQWlELFVBQU0sRUFBQyxRQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtEQUFSLENBQUgsQ0FMSixDQURKLEVBUUk7QUFBSyxhQUFTLEVBQUMsZ0ZBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUssYUFBUyxFQUFDLDJCQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFESixFQUVJO0FBQVEsU0FBSyxFQUFDLEtBQWQ7QUFBb0IsVUFBTSxFQUFDLEtBQTNCO0FBQWlDLE9BQUcsRUFBQyx5REFBckM7QUFBK0YsZUFBVyxFQUFDLEdBQTNHO0FBQStHLFNBQUssRUFBQywwRkFBckg7QUFBZ04sbUJBQWUsTUFBL047QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUZKLENBUkosQ0FESixDQURKLENBL0dKLEVBaUlJO0FBQVMsYUFBUyxZQUFNQSxhQUFhLEdBQUcsbUJBQUgsR0FBeUIsaUJBQTVDLGFBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxXQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFJLGFBQVMsRUFBQyxNQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREosRUFHSTtBQUFHLFFBQUksRUFBQywwQ0FBUjtBQUFtRCxhQUFTLEVBQUMsaUJBQTdEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEseUNBSEosQ0FESixDQWpJSixFQXlJSTtBQUFRLGFBQVMsWUFBTUEsYUFBYSxHQUFHLGVBQUgsR0FBcUIsYUFBeEMsaUJBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyxXQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFLLGFBQVMsRUFBQyx3REFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBRyxRQUFJLEVBQUMsd0NBQVI7QUFBaUQsVUFBTSxFQUFDLFFBQXhEO0FBQWlFLGFBQVMsRUFBQywrQkFBM0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUEyRyxNQUFDLDZEQUFEO0FBQWdCLFFBQUksRUFBRyxFQUF2QjtBQUE0QixTQUFLLEVBQUc7QUFBRVMsV0FBSyxFQUFFO0FBQVQsS0FBcEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUEzRyxDQURKLEVBRUk7QUFBRyxRQUFJLEVBQUdELHlEQUFWO0FBQXNDLFVBQU0sRUFBQyxRQUE3QztBQUFzRCxhQUFTLEVBQUMsK0JBQWhFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBZ0csTUFBQywwREFBRDtBQUFhLFFBQUksRUFBRyxFQUFwQjtBQUF5QixTQUFLLEVBQUc7QUFBRUMsV0FBSyxFQUFFO0FBQVQsS0FBakM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUFoRyxDQUZKLEVBR0k7QUFBRyxRQUFJLEVBQUdELDBEQUFWO0FBQXVDLFVBQU0sRUFBQyxRQUE5QztBQUF1RCxhQUFTLEVBQUMsK0JBQWpFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBaUcsTUFBQywwREFBRDtBQUFhLFFBQUksRUFBRyxFQUFwQjtBQUF5QixTQUFLLEVBQUc7QUFBRUMsV0FBSyxFQUFFO0FBQVQsS0FBakM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUFqRyxDQUhKLEVBSUk7QUFBRyxRQUFJLEVBQUdELHNEQUFWO0FBQXNDLFVBQU0sRUFBQyxRQUE3QztBQUFzRCxhQUFTLEVBQUMsK0JBQWhFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBZ0csTUFBQywyREFBRDtBQUFjLFFBQUksRUFBRyxFQUFyQjtBQUEwQixTQUFLLEVBQUc7QUFBRUMsV0FBSyxFQUFFO0FBQVQsS0FBbEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUFoRyxDQUpKLEVBS0k7QUFBRyxRQUFJLEVBQUdELDJEQUFWO0FBQXFDLFVBQU0sRUFBQyxRQUE1QztBQUFxRCxhQUFTLEVBQUMsK0JBQS9EO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBK0YsTUFBQyx3REFBRDtBQUFXLFFBQUksRUFBRyxFQUFsQjtBQUF1QixTQUFLLEVBQUc7QUFBRUMsV0FBSyxFQUFFO0FBQVQsS0FBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUEvRixDQUxKLENBREosQ0FESixDQXpJSixDQURKO0FBdUpIOztHQTlKdUJaLEk7O0tBQUFBLEkiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguZTZmMTBhMDQ1ZTRhZmY5YmU2YzAuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VDb250ZXh0LCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCc7XHJcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluayc7XHJcblxyXG5pbXBvcnQgeyBDb250ZXh0IH0gZnJvbSAnLi4vY29udGV4dC9BdXRoQ29udGV4dCc7XHJcblxyXG5pbXBvcnQgUmVhY3RHQSBmcm9tICdyZWFjdC1nYSc7XHJcbmltcG9ydCB7IEZhRmFjZWJvb2tGLCBGYUxpbmtlZGluSW4sIEZhSW5zdGFncmFtLCBGYVlvdXR1YmUsIEZhTW91c2VQb2ludGVyIH0gZnJvbSBcInJlYWN0LWljb25zL2ZhXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb21lKCkge1xyXG4gICAgY29uc3QgeyBhdXRoZW50aWNhdGVkIH0gPSB1c2VDb250ZXh0KCBDb250ZXh0ICk7XHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgUmVhY3RHQS5wYWdldmlldyggd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lICsgd2luZG93LmxvY2F0aW9uLnNlYXJjaCApO1xyXG4gICAgfSwgW10gKTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgICAgICAgPHRpdGxlPnsgcHJvY2Vzcy5lbnYuQVBQX05BTUUgfTwvdGl0bGU+XHJcbiAgICAgICAgICAgIDwvSGVhZD5cclxuXHJcbiAgICAgICAgICAgIDxzZWN0aW9uIGlkPVwiYmFubmVyXCI+XHJcbiAgICAgICAgICAgICAgICB7IGF1dGhlbnRpY2F0ZWQgJiYgPGltZyBzcmM9XCIuL2ltYWdlcy9iYW5uZXItbG9nYWRvLmpwZ1wiIGNsYXNzTmFtZT1cImltZy1mbHVpZFwiIGFsdD1cIkJhbm5lciBsb2dhZG9cIiAvPiB9XHJcbiAgICAgICAgICAgICAgICB7ICFhdXRoZW50aWNhdGVkICYmIDxpbWcgc3JjPVwiLi9pbWFnZXMvYmFubmVyLmpwZ1wiIGNsYXNzTmFtZT1cImltZy1mbHVpZFwiIGFsdD1cIkJhbm5lclwiIC8+IH1cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG5cclxuICAgICAgICAgICAgeyFhdXRoZW50aWNhdGVkICYmIDxzZWN0aW9uIGlkPVwiZGVzY3JpcHRpb25cIiBjbGFzc05hbWU9XCJzaGFkb3ctc21cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLXJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0xMiBjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cInRleHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwidGV4dC11cHBlcmNhc2UgbWItM1wiPkJFTS1WSU5ETyhBKSE8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIm1iLTBcIj5Tb21vcyBvIExhYm9yYXTDs3JpbyBJbnRlbGlnw6puY2lhIGRlIFZpZGEsIHByb2dyYW1hIHF1ZSBkZXNlbnZvbHZlIG8gcGlsYXIgc29jaW9lbW9jaW9uYWwgbmFzIGVzY29sYXMgZGUgdG9kbyBvIEJyYXNpbC4gQ3JpYW1vcyBlc3Bhw6dvcyBkZSBmYWxhIGUgZGUgZXNjdXRhIHBhcmEgYW1wbGlhciBhIGNvbXByZWVuc8OjbyBxdWUgb3MgYWx1bm9zIHTDqm0gZGUgc2ksIGRvIG91dHJvIGUgZG8gbXVuZG8uPC9wPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5Fc3NhIMOpIGEgcGxhdGFmb3JtYSAzRCBcIlNlamEgTElWXCIsIG5vc3NhIGV4cGVyacOqbmNpYSBpbm92YWRvcmEgZSBleGNsdXNpdmEuIEFxdWksIGEgY29tdW5pZGFkZSBlc2NvbGFyIHNlIHJlw7puZSBlIHZpdmVuY2lhIG9zIGN1cnLDrWN1bG9zIGRvIG5vc3NvIHByb2dyYW1hIHNvY2lvZW1vY2lvbmFsLiBWb2PDqlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvZGUgbmF2ZWdhciBqdW50byBjb20gb3MgcGVyc29uYWdlbnMsIHZpc2l0YXIgYSBjYXNhIGFtYXJlbGEsIGxlciBhcyBoaXN0w7NyaWFzIGRvIFRvbcOhcywgY29uaGVjZXIgbm9zc29zIGpvZ29zIGUgc2VyaWFkb3MgZSBtdWl0byBtYWlzIVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5BY2Vzc2UgYWdvcmEgZSB2aXZlbmNpZSB1bWEgI0VzY29sYVF1ZVNlbnRlIGNvbSBvIExJViE8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0xMiBjb2wtbWQtNiBhbGlnbi1zZWxmLWNlbnRlciB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidG5zLWFjZXNzby1ldmVudG9cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGluayBocmVmPVwiL2ZhY2hhZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiYnRuIGJ0bi1wbGF0YWZvcm1hLTNkIGJ0bi1sZ1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHN0cm9uZz5BQ0VTU0FSIEEgUExBVEFGT1JNQTwvc3Ryb25nPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9MaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKiA8TGluayBocmVmPVwiL3JlZ2lzdGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImJ0biBidG4tcGxhdGFmb3JtYS0zZCBidG4tbGdcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+Q0FEQVNUUkUtU0U8L3N0cm9uZz4gUEFSQSA8YnIgLz5BQ0VTU0FSIEEgUExBVEFGT1JNQVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9MaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMaW5rIGhyZWY9XCIvbG9naW5cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiYnRuIGJ0bi1mYXEgYnRuLWxnIHNoYWRvdy1zbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwibS0wIHRleHQtY2VudGVyXCI+T1UgPHN0cm9uZz5FTlRSRSBQT1IgQVFVSTwvc3Ryb25nPiwgPGJyIC8+IENBU08gSsOBIFRFTkhBIENBREFTVFJPITwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz4gKi99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPiB9XHJcblxyXG4gICAgICAgICAgICB7YXV0aGVudGljYXRlZCAmJiA8c2VjdGlvbiBpZD1cImRlc2NyaXB0aW9uLWxvZ2Fkb1wiIGNsYXNzTmFtZT1cInNoYWRvdy1zbVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWZsZXggZmxleC1jb2x1bW4gZmxleC1tZC1yb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJnLWJsdWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtMTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwidGV4dC11cHBlcmNhc2UgbWItM1wiPlFVRSBCT00gVEVSIFZPQ8OKIEFRVUkhPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPlNvbW9zIG8gTGFib3JhdMOzcmlvIEludGVsaWfDqm5jaWEgZGUgVmlkYSwgcHJvZ3JhbWEgcXVlIGRlc2Vudm9sdmUgbyBwaWxhciBzb2Npb2Vtb2Npb25hbCBuYXMgZXNjb2xhcyBkZSB0b2RvIG8gQnJhc2lsLiBDcmlhbW9zIGVzcGHDp29zIGRlIGZhbGEgZSBkZSBlc2N1dGEgcGFyYSBhbXBsaWFyIGEgY29tcHJlZW5zw6NvIHF1ZSBvcyBhbHVub3MgdMOqbSBkZSBzaSwgZG8gb3V0cm8gZSBkbyBtdW5kby48L3A+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+QW50ZXMgZGUgdHVkbywgcXVlIHRhbCBjb25oZWNlciB1bSBwb3VjbyBkYSAjRXNjb2xhUXVlU2VudGU/IEFvIGNsaWNhciBlbSBcIkFjZXNzZSBhIHBsYXRhZm9ybWFcIiwgdm9jw6ogY29uaGVjZXLDoSBhIGZhY2hhZGEgZGEgbm9zc2EgZXNjb2xhLiBDbGljYW5kbyBuYSBwb3J0YVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGUgZW50cmFkYSwgdm9jw6ogw6kgbm9zc28gY29udmlkYWRvIHBhcmEgZXhwbG9yYXIgbyBsb3VuZ2UsIGVzcGHDp28gY29tIGRpZmVyZW50ZXMgY29udGXDumRvcyBlIGV4cGVyacOqbmNpYXMgZGl2aWRpZG9zIGVtIHBvcnRhcyBjb2xvcmlkYXMuPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJiZy15ZWxsb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtMTIgYWxpZ24tc2VsZi1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJibG9jby1hY2Vzc2UtcGxhdGFmb3JtYSBwdC1tZC0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGluayBocmVmPVwiL2ZhY2hhZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJidG4gYnRuLXBsYXRhZm9ybWEtM2QgZm9udC13ZWlnaHQtYm9sZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBBQ0VTU0UgQSBQTEFUQUZPUk1BXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9MaW5rPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPkVzc2UgZXNwYcOnbyDDqSBzZXUhIFBvcnRhbnRvLCBmaXF1ZSDDoCB2b250YWRlIHBhcmEgY2xpY2FyIG5hcyBwb3J0YXMgZSByZXRvcm5hciAodXNhbmRvIGEgc2V0YSBkbyBzZXUgbmF2ZWdhZG9yKSBhIHF1YWxxdWVyIG1vbWVudG8uPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPjxzdHJvbmc+RGljYTo8L3N0cm9uZz4gQSBwb3J0YSBcIkV4cG9zacOnw6NvIExJVlwiIMOpIG8gYW1iaWVudGUgbWFpcyBkaXZlcnNvIGUgaW50ZXJhdGl2byBxdWUgdm9jw6ogdmFpIGVuY29udHJhciEgTmVsZSwgcG9kZXLDoSBjb25oZWNlciBtYWlzIGRldGFsaGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGEgZXhwZXJpw6puY2lhIExJViwgZGVzZGUgYSBFZHVjYcOnw6NvIEluZmFudGlsIGF0w6kgbyBFbnNpbm8gTcOpZGlvLCBpbmNsdWluZG8gbyBwcm9ncmFtYSBMSVYrLCBhIFBsYXRhZm9ybWEgZGUgRW5zaW5vIEVsZXZhIGUgdW0gZXNwYcOnbyBwYXJhIGNvbnZlcnNhciBjb20gbm9zc29zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3VsdG9yZXMuIEFzIMOhcmVhcyBjbGljw6F2ZWlzIGVzdGFyw6NvIHNlbXByZSBwaXNjYW5kbyBuYSB0ZWxhLiA8c3Ryb25nPkNvbXBsZXRhbWVudGUgaW1wZXJkw612ZWwhPC9zdHJvbmc+PC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvc2VjdGlvbj4gfVxyXG5cclxuICAgICAgICAgICAgPHNlY3Rpb24gaWQ9XCJiYW5uZXItaG9tZS1sb2JieVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvdyBkLWZsZXgganVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGluayBocmVmPVwiL2ZhY2hhZGFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjPVwiLi9pbWFnZXMvYmFubmVyLWVzY29sYS1xdWUtc2VudGUuanBnXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PVwiYmFubmVyLWVzY29sYS1xdWUtc2VudGVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJpbWctZmx1aWRcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLTEyIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyIGJsb2NvLWJhbm5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17IGAke2F1dGhlbnRpY2F0ZWQgPyAnYmFubmVyLXRleHRvLWVzY29sYS1sb2dhZG8nIDogJ2Jhbm5lci10ZXh0by1lc2NvbGEnfSB0ZXh0LWNlbnRlciBweS0zIGNvbC1tZC04YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwibWItMFwiPlZJVkVOQ0lFIEEgPHN0cm9uZz4jRVNDT0xBUVVFU0VOVEU8L3N0cm9uZz4gSlVOVE8gQU8gUFJPR1JBTUEgPGJyIC8+IFFVRSBEw4EgVk9aIMOAUyBFTU/Dh8OVRVMgREUgQUxVTk9TIFBFTE8gQlJBU0lMISA8L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG5cclxuICAgICAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPXsgYCR7YXV0aGVudGljYXRlZCA/ICdtZWV0LWJnLXllbGxvdycgOiAnbWVldC1iZy1ibHVlJ30gbWVldGAgfT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLXJvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0xMiBjb2wtbWQtNiBwci1tZC00XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwibWItM1wiPkNPTkhFw4dBIE8gTElWITwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5PIExJViDDqSBvIHByb2dyYW1hIHNvY2lvZW1vY2lvbmFsIHF1ZSBlc3TDoSBwcmVzZW50ZSBlbSBtYWlzIGRlIDM1MCBpbnN0aXR1acOnw7VlcyBkZSBlbnNpbm8gcGFyY2VpcmFzIGNvbSAyMDAgbWlsIGFsdW5vcyBlIGZhbcOtbGlhcywgZGEgRWR1Y2HDp8OjbyBJbmZhbnRpbCBhbyBFbnNpbm8gTcOpZGlvLCBidXNjYW5kbyBxdWUgb3NcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVzdHVkYW50ZXMgcG9zc2FtIGNvbmhlY2VyIHNldXMgc2VudGltZW50b3MgZSBkZXNlbnZvbHZlciBoYWJpbGlkYWRlcyBwYXJhIGEgdmlkYS5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPjxzdHJvbmc+PGEgaHJlZj1cImh0dHBzOi8vd3d3LmludGVsaWdlbmNpYWRldmlkYS5jb20uYnIvXCIgdGFyZ2V0PVwiX2JsYW5rXCI+Q0xJUVVFIEFRVUkgRSBDT05IRcOHQSBNQUlTIFNPQlJFIE8gTElWITwvYT48L3N0cm9uZz48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0xMiBjb2wtbWQtNiBhbGlnbi1zZWxmLWNlbnRlciB0ZXh0LWNlbnRlciB0ZXh0LW1kLXJpZ2h0IGJsb2NvLXZpZGVvLWlmcmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJibG9jby12aWRlby1pZnJhbWUtdHJhY29zXCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aWZyYW1lIHdpZHRoPVwiOTMlXCIgaGVpZ2h0PVwiMzE1XCIgc3JjPVwiaHR0cHM6Ly93d3cueW91dHViZS5jb20vZW1iZWQvMlpTLTFXQXd3LTA/bXV0ZT0xJmxvb3A9MVwiIGZyYW1lQm9yZGVyPVwiMFwiIGFsbG93PVwiYWNjZWxlcm9tZXRlcjsgYXV0b3BsYXk7IGNsaXBib2FyZC13cml0ZTsgZW5jcnlwdGVkLW1lZGlhOyBneXJvc2NvcGU7IHBpY3R1cmUtaW4tcGljdHVyZVwiIGFsbG93RnVsbFNjcmVlbj48L2lmcmFtZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG5cclxuICAgICAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPXsgYCR7YXV0aGVudGljYXRlZCA/ICdjb250YWN0LWJnLXllbGxvdycgOiAnY29udGFjdC1iZy1ibHVlJ30gY29udGFjdGAgfSA+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoMiBjbGFzc05hbWU9XCJtYi0zXCI+RMOaVklEQVM/PC9oMj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cIm1haWx0bzpjb250YXRvQGludGVsaWdlbmNpYWRldmlkYS5jb20uYnJcIiBjbGFzc05hbWU9XCJidG4gYnRuLWNvbnRhdG9cIj5DT05UQVRPQElOVEVMSUdFTkNJQURFVklEQS5DT00uQlI8L2E+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG5cclxuICAgICAgICAgICAgPGZvb3RlciBjbGFzc05hbWU9eyBgJHthdXRoZW50aWNhdGVkID8gJ2Zvb3Rlci15ZWxsb3cnIDogJ2Zvb3Rlci1ibHVlJ30gdGV4dC1jZW50ZXJgIH0+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXIganVzdGlmeS1jb250ZW50LW1kLXN0YXJ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJodHRwczovL3d3dy5pbnRlbGlnZW5jaWFkZXZpZGEuY29tLmJyL1wiIHRhcmdldD1cIl9ibGFua1wiIGNsYXNzTmFtZT1cImJ0biBidG4tc29jaWFsIHNoYWRvdy1zbSBteC0yXCI+PEZhTW91c2VQb2ludGVyIHNpemU9eyAyMiB9IHN0eWxlPXsgeyBjb2xvcjogJyNmZmYnIH0gfSAvPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj17IHByb2Nlc3MuZW52LkxJTktfRkFDRUJPT0sgfSB0YXJnZXQ9XCJfYmxhbmtcIiBjbGFzc05hbWU9XCJidG4gYnRuLXNvY2lhbCBzaGFkb3ctc20gbXgtMlwiPjxGYUZhY2Vib29rRiBzaXplPXsgMjIgfSBzdHlsZT17IHsgY29sb3I6ICcjZmZmJyB9IH0gLz48L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9eyBwcm9jZXNzLmVudi5MSU5LX0lOU1RBR1JBTSB9IHRhcmdldD1cIl9ibGFua1wiIGNsYXNzTmFtZT1cImJ0biBidG4tc29jaWFsIHNoYWRvdy1zbSBteC0yXCI+PEZhSW5zdGFncmFtIHNpemU9eyAyMiB9IHN0eWxlPXsgeyBjb2xvcjogJyNmZmYnIH0gfSAvPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj17IHByb2Nlc3MuZW52LkxJTktfTElOS0VESU4gfSB0YXJnZXQ9XCJfYmxhbmtcIiBjbGFzc05hbWU9XCJidG4gYnRuLXNvY2lhbCBzaGFkb3ctc20gbXgtMlwiPjxGYUxpbmtlZGluSW4gc2l6ZT17IDIyIH0gc3R5bGU9eyB7IGNvbG9yOiAnI2ZmZicgfSB9IC8+PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPXsgcHJvY2Vzcy5lbnYuTElOS19ZT1VUVUJFIH0gdGFyZ2V0PVwiX2JsYW5rXCIgY2xhc3NOYW1lPVwiYnRuIGJ0bi1zb2NpYWwgc2hhZG93LXNtIG14LTJcIj48RmFZb3V0dWJlIHNpemU9eyAyMiB9IHN0eWxlPXsgeyBjb2xvcjogJyNmZmYnIH0gfSAvPjwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Zvb3Rlcj5cclxuICAgICAgICA8Lz5cclxuICAgIClcclxufSJdLCJzb3VyY2VSb290IjoiIn0=