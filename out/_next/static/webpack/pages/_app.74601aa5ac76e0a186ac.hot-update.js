webpackHotUpdate_N_E("pages/_app",{

/***/ "./context/AuthContext.js":
/*!********************************!*\
  !*** ./context/AuthContext.js ***!
  \********************************/
/*! exports provided: Context, AuthProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return Context; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthProvider", function() { return AuthProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-ga */ "./node_modules/react-ga/dist/esm/index.js");
var _jsxFileName = "C:\\git\\nx_liv\\context\\AuthContext.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



react_ga__WEBPACK_IMPORTED_MODULE_2__["default"].initialize('UA-98919187-4');
var Context = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();

function AuthProvider(_ref) {
  _s();

  var children = _ref.children;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"])();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      user = _useState[0],
      setUser = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      authenticated = _useState2[0],
      setAuthenticated = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      token = _useState3[0],
      setToken = _useState3[1];

  var checkAuthenticated = function checkAuthenticated() {
    var userSession = JSON.parse(localStorage.getItem('user')); // if(userSession){
    //     setAuthenticated(true);
    //     setUser(userSession);
    //     if(router.route != '/' && router.route != '/validar-email'){
    //         if(userSession.email_verified_at == null || userSession.email_verified_at == ''){
    //             router.push('/verificar-email');
    //         }
    //     }
    //     if(router.route == '/login' || router.route == '/register'){
    //         router.push('/');
    //     }
    // }else{
    //     const urls = ['/', '/register', '/login', '/resetar-senha'];
    //     setAuthenticated(false);
    //     setUser(null);
    //     if(!urls.includes(router.pathname)) router.push('/login');
    // }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    checkAuthenticated();
  }, []);
  return __jsx(Context.Provider, {
    value: {
      user: user,
      setUser: setUser,
      authenticated: authenticated,
      setAuthenticated: setAuthenticated,
      checkAuthenticated: checkAuthenticated
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 9
    }
  }, children);
}

_s(AuthProvider, "BGHxsBJBXZFVmLbg0BZnaLheESM=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"]];
});

_c = AuthProvider;


var _c;

$RefreshReg$(_c, "AuthProvider");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29udGV4dC9BdXRoQ29udGV4dC5qcyJdLCJuYW1lcyI6WyJSZWFjdEdBIiwiaW5pdGlhbGl6ZSIsIkNvbnRleHQiLCJjcmVhdGVDb250ZXh0IiwiQXV0aFByb3ZpZGVyIiwiY2hpbGRyZW4iLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ1c2VTdGF0ZSIsInVzZXIiLCJzZXRVc2VyIiwiYXV0aGVudGljYXRlZCIsInNldEF1dGhlbnRpY2F0ZWQiLCJ0b2tlbiIsInNldFRva2VuIiwiY2hlY2tBdXRoZW50aWNhdGVkIiwidXNlclNlc3Npb24iLCJKU09OIiwicGFyc2UiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwidXNlRWZmZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUFBLGdEQUFPLENBQUNDLFVBQVIsQ0FBbUIsZUFBbkI7QUFFQSxJQUFNQyxPQUFPLGdCQUFHQywyREFBYSxFQUE3Qjs7QUFFQSxTQUFTQyxZQUFULE9BQWlDO0FBQUE7O0FBQUEsTUFBVkMsUUFBVSxRQUFWQSxRQUFVO0FBQzdCLE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7O0FBRDZCLGtCQUVMQyxzREFBUSxDQUFDLElBQUQsQ0FGSDtBQUFBLE1BRXRCQyxJQUZzQjtBQUFBLE1BRWhCQyxPQUZnQjs7QUFBQSxtQkFHYUYsc0RBQVEsQ0FBQyxLQUFELENBSHJCO0FBQUEsTUFHdEJHLGFBSHNCO0FBQUEsTUFHUEMsZ0JBSE87O0FBQUEsbUJBSUhKLHNEQUFRLENBQUMsSUFBRCxDQUpMO0FBQUEsTUFJdEJLLEtBSnNCO0FBQUEsTUFJZkMsUUFKZTs7QUFNN0IsTUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixHQUFNO0FBQzdCLFFBQU1DLFdBQVcsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixNQUFyQixDQUFYLENBQXBCLENBRDZCLENBRzdCO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDSCxHQXpCRDs7QUEyQkFDLHlEQUFTLENBQUMsWUFBTTtBQUNaTixzQkFBa0I7QUFDckIsR0FGUSxFQUVOLEVBRk0sQ0FBVDtBQUlBLFNBQ0ksTUFBQyxPQUFELENBQVMsUUFBVDtBQUFrQixTQUFLLEVBQUU7QUFDckJOLFVBQUksRUFBSkEsSUFEcUI7QUFFckJDLGFBQU8sRUFBUEEsT0FGcUI7QUFHckJDLG1CQUFhLEVBQWJBLGFBSHFCO0FBSXJCQyxzQkFBZ0IsRUFBaEJBLGdCQUpxQjtBQUtyQkcsd0JBQWtCLEVBQWxCQTtBQUxxQixLQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBT0tWLFFBUEwsQ0FESjtBQVdIOztHQWhEUUQsWTtVQUNVRyxxRDs7O0tBRFZILFk7QUFrRFQiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC43NDYwMWFhNWFjNzZlMGExODZhYy5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7Y3JlYXRlQ29udGV4dCwgdXNlU3RhdGUsIHVzZUVmZmVjdH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XHJcbmltcG9ydCBSZWFjdEdBIGZyb20gJ3JlYWN0LWdhJztcclxuXHJcblJlYWN0R0EuaW5pdGlhbGl6ZSgnVUEtOTg5MTkxODctNCcpO1xyXG5cclxuY29uc3QgQ29udGV4dCA9IGNyZWF0ZUNvbnRleHQoKTtcclxuXHJcbmZ1bmN0aW9uIEF1dGhQcm92aWRlcih7Y2hpbGRyZW59KXtcclxuICAgIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gICAgY29uc3QgW3VzZXIsIHNldFVzZXJdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgICBjb25zdCBbYXV0aGVudGljYXRlZCwgc2V0QXV0aGVudGljYXRlZF0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgICBjb25zdCBbdG9rZW4sIHNldFRva2VuXSA9IHVzZVN0YXRlKG51bGwpO1xyXG5cclxuICAgIGNvbnN0IGNoZWNrQXV0aGVudGljYXRlZCA9ICgpID0+IHtcclxuICAgICAgICBjb25zdCB1c2VyU2Vzc2lvbiA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXInKSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gaWYodXNlclNlc3Npb24pe1xyXG4gICAgICAgIC8vICAgICBzZXRBdXRoZW50aWNhdGVkKHRydWUpO1xyXG4gICAgICAgIC8vICAgICBzZXRVc2VyKHVzZXJTZXNzaW9uKTtcclxuXHJcbiAgICAgICAgLy8gICAgIGlmKHJvdXRlci5yb3V0ZSAhPSAnLycgJiYgcm91dGVyLnJvdXRlICE9ICcvdmFsaWRhci1lbWFpbCcpe1xyXG4gICAgICAgIC8vICAgICAgICAgaWYodXNlclNlc3Npb24uZW1haWxfdmVyaWZpZWRfYXQgPT0gbnVsbCB8fCB1c2VyU2Vzc2lvbi5lbWFpbF92ZXJpZmllZF9hdCA9PSAnJyl7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgcm91dGVyLnB1c2goJy92ZXJpZmljYXItZW1haWwnKTtcclxuICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgICAgICAvLyAgICAgaWYocm91dGVyLnJvdXRlID09ICcvbG9naW4nIHx8IHJvdXRlci5yb3V0ZSA9PSAnL3JlZ2lzdGVyJyl7XHJcbiAgICAgICAgLy8gICAgICAgICByb3V0ZXIucHVzaCgnLycpO1xyXG4gICAgICAgIC8vICAgICB9XHJcblxyXG4gICAgICAgIC8vIH1lbHNle1xyXG4gICAgICAgIC8vICAgICBjb25zdCB1cmxzID0gWycvJywgJy9yZWdpc3RlcicsICcvbG9naW4nLCAnL3Jlc2V0YXItc2VuaGEnXTtcclxuXHJcbiAgICAgICAgLy8gICAgIHNldEF1dGhlbnRpY2F0ZWQoZmFsc2UpO1xyXG4gICAgICAgIC8vICAgICBzZXRVc2VyKG51bGwpO1xyXG5cclxuICAgICAgICAvLyAgICAgaWYoIXVybHMuaW5jbHVkZXMocm91dGVyLnBhdGhuYW1lKSkgcm91dGVyLnB1c2goJy9sb2dpbicpO1xyXG4gICAgICAgIC8vIH1cclxuICAgIH1cclxuXHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIGNoZWNrQXV0aGVudGljYXRlZCgpO1xyXG4gICAgfSwgW10pO1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3tcclxuICAgICAgICAgICAgdXNlcixcclxuICAgICAgICAgICAgc2V0VXNlcixcclxuICAgICAgICAgICAgYXV0aGVudGljYXRlZCxcclxuICAgICAgICAgICAgc2V0QXV0aGVudGljYXRlZCxcclxuICAgICAgICAgICAgY2hlY2tBdXRoZW50aWNhdGVkXHJcbiAgICAgICAgfX0+XHJcbiAgICAgICAgICAgIHtjaGlsZHJlbn1cclxuICAgICAgICA8L0NvbnRleHQuUHJvdmlkZXI+XHJcbiAgICApO1xyXG59XHJcblxyXG5leHBvcnQge0NvbnRleHQsIEF1dGhQcm92aWRlcn07Il0sInNvdXJjZVJvb3QiOiIifQ==