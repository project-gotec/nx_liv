import React, {createContext, useState, useEffect} from 'react';
import { useRouter } from 'next/router';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-98919187-4');

const Context = createContext();

function AuthProvider({children}){
    const router = useRouter();
    const [user, setUser] = useState(null);
    const [authenticated, setAuthenticated] = useState(false);
    const [token, setToken] = useState(null);

    const checkAuthenticated = () => {
        const userSession = JSON.parse(localStorage.getItem('user'));
        
        // if(userSession){
        //     setAuthenticated(true);
        //     setUser(userSession);

        //     if(router.route != '/' && router.route != '/validar-email'){
        //         if(userSession.email_verified_at == null || userSession.email_verified_at == ''){
        //             router.push('/verificar-email');
        //         }
        //     }
            
        //     if(router.route == '/login' || router.route == '/register'){
        //         router.push('/');
        //     }

        // }else{
        //     const urls = ['/', '/register', '/login', '/resetar-senha'];

        //     setAuthenticated(false);
        //     setUser(null);

        //     if(!urls.includes(router.pathname)) router.push('/login');
        // }
    }

    useEffect(() => {
        checkAuthenticated();
    }, []);

    return (
        <Context.Provider value={{
            user,
            setUser,
            authenticated,
            setAuthenticated,
            checkAuthenticated
        }}>
            {children}
        </Context.Provider>
    );
}

export {Context, AuthProvider};